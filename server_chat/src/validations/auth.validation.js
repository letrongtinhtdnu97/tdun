const { validate, ValidationError, Joi } = require('express-validation')

module.exports = {
    register: {
        body: Joi.object({
            email: Joi.string()
                .email()
                .required(),
            password: Joi.string()
                .required()
                .min(6),
            firstname: Joi.string()
                .min(1)
                .max(255)
                .required(),
            lastname: Joi.string()
                .min(1)
                .max(255)
                .required(),
        })
    },
    login: {
        body: Joi.object({
            email: Joi.string()
                .email()
                .required(),
            password: Joi.string()
                .min(6)
                .required()
        })
    },
    refresh: {
        body: Joi.object({
            email: Joi.string()
                .email()
                .required(),
            refreshToken: Joi.string().required()
        })
    },
    // POST /v1/auth/refresh
    sendPasswordReset: {
        body: Joi.object({
            email: Joi.string()
                .email()
                .required()
            })
    },

    // POST /v1/auth/password-reset
    passwordReset: {
        body: Joi.object({
            email: Joi.string()
                .email()
                .required(),
            password: Joi.string()
                .required()
                .min(6)
                .max(128),
            resetToken: Joi.string().required()
        })
    },
    oAuth: {
        body: Joi.object({
          access_token: Joi.string().required()
        })
    },
}