/* eslint-disable arrow-body-style */
const request = require('supertest');
const httpStatus = require('http-status');
const { expect } = require('chai');
const sinon = require('sinon');
const moment = require('moment-timezone');
const app = require('../index');
const User = require('../models/User.models');
const RefreshToken = require('../models/RefreshToken.model');
const PasswordResetToken = require('../models/PasswordResetToken.model');
const authProviders = require('../services/authProviders');
const emailProvider = require('../services/emails/emailProvider');

const sandbox = sinon.createSandbox();

const fakeOAuthRequest = () =>
  Promise.resolve({
    service: 'facebook',
    id: '123',
    name: 'user',
    email: 'test@test.com',
    picture: 'test.jpg',
  });

describe('Authentication API', () => {
  let dbUser;
  let user;
  let user2;
  let user3;
  let refreshToken;
  let resetToken;
  let expiredRefreshToken;
  let expiredResetToken;
  let user4;

  beforeEach(async () => {
    dbUser = {
      email: 'tinh@gmail.com',
      password: '123456',
      lastname: 'Le',
      firstname: 'Tinh',
      role: 'admin',
    };
    user4 = {
        email: 'tinh@gmail.com',
        password: '123456',
        firstname: 'Tinh',
        lastname: 'Le'
    }

    user = {
      email: 'sousa.dfs@gmail.com',
      password: '123456',
      firstname: 'Sousa',
      lastname: 'Daniel'
    };
    user2 = {
        email: 'sousa.dfs@gmail.com',
        password: '123456',
        firstname: 'Sousa',
        lastname: 'Daniel',
        role: 'admin',
    };
    user3 = {
        email: 'sousa.dfs3',
        password: '1234563',
        firstname: 'Sousa',
        lastname: 'Daniel',
        role: 'admin',
    };

    refreshToken = {
      token:
        '5947397b323ae82d8c3a333b.c69d0435e62c9f4953af912442a3d064e20291f0d228c0552ed4be473e7d191ba40b18c2c47e8b9d',
      userId: '5947397b323ae82d8c3a333b',
      userEmail: dbUser.email,
      expires: moment()
        .add(1, 'day')
        .toDate(),
    };

    resetToken = {
      resetToken:
        '5947397b323ae82d8c3a333b.c69d0435e62c9f4953af912442a3d064e20291f0d228c0552ed4be473e7d191ba40b18c2c47e8b9d',
      userId: '5947397b323ae82d8c3a333b',
      userEmail: dbUser.email,
      expires: moment()
        .add(2, 'hours')
        .toDate(),
    };

    expiredRefreshToken = {
      token:
        '5947397b323ae82d8c3a333b.c69d0435e62c9f4953af912442a3d064e20291f0d228c0552ed4be473e7d191ba40b18c2c47e8b9d',
      userId: '5947397b323ae82d8c3a333b',
      userEmail: dbUser.email,
      expires: moment()
        .subtract(1, 'day')
        .toDate(),
    };

    expiredResetToken = {
      resetToken:
        '5947397b323ae82d8c3a333b.c69d0435e62c9f4953af912442a3d064e20291f0d228c0552ed4be473e7d191ba40b18c2c47e8b9d',
      userId: '5947397b323ae82d8c3a333b',
      userEmail: dbUser.email,
      expires: moment()
        .subtract(2, 'hours')
        .toDate(),
    };

    await User.deleteMany({});
    await User.create(dbUser);
    await RefreshToken.deleteMany({});
    await PasswordResetToken.deleteMany({});
  });

  afterEach(() => sandbox.restore());
  describe('POST /v1/auth/register',() => {
      it('should register a new user when request is ok', () => {
          return request(app)
            .post('/v1/auth/register')
            .send(user)
            .expect(httpStatus.CREATED)
            .then((res) => {
                expect(res.body.token).to.have.a.property('accessToken');
                expect(res.body.token).to.have.a.property('refreshToken');
                expect(res.body.token).to.have.a.property('expiresIn');
            })
      })
      it('should report error when email already exists', () => {
          return request(app)
            .post('/v1/auth/register')
            .send(user4)
            .expect(httpStatus.CONFLICT)
            .then((res) => {
                const { field } = res.body.errors[0];
                expect(field).to.be.equal('email');
            })
      })
      it('should report error when the email provided is not valid',() => {
          user4.email = 'this_is_not_an_email';
          return request(app)
            .post('/v1/auth/register')
            .send(user4)
            .expect(httpStatus.INTERNAL_SERVER_ERROR)
            .then((res) => {
                const {code} = res.body   
                expect(code).to.be.a('number')
                expect(res.body).to.be.a('object');
            })
      })
  })
  
});
