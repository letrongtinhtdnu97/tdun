const httpStatus = require('http-status');

const { jwtExpirationInterval } = require('../configs/config')
const moment = require('moment');
const { omit } = require('lodash');
const IError = require('../utils/IError')
const logger = require('../configs/logger')
//models
const Classes = require('../models/Class.model')
const Student  = require('../models/Student.model')
const ScheduleModel = require('../models/Schedule.model')
const CreateNew = async(req,res,next) => {
    const {id,name, subName, startAt, endAt} = req.body
    const classNew = new Classes({name,subName,startAt,endAt})
    try {
        const foundClass = await Classes.findOne({name: name})
        if(foundClass) {
            return res.status(403).json({
                success: false,
                message: "found"
            })
        }
        await classNew.save()
        logger.info(`${id} create class ${classNew}`)
        return res.status(200).json({
            success: true
        })
    } catch (error) {
        logger.error(`${id} no create new class ${classNew}`)
        return res.status(500).json({
            success: false
        })
    }
}
const getAllSchedule = async(req, res,next) => {
    const {id} = req.body

    try {
        const getClass = await Student.findOne({userId: id})
        if(!getClass) {
            return res.status(400).json({
                success: false,
                success: 'no class'
            })
        }
        const getSchedule = await ScheduleModel.find({classId: getClass.classId})
        if(!getSchedule) {
            return res.status(200).json({
                success: true,
                data: []
            })
        }
       
        
        console.log(getSchedule)
        return res.status(200).json({
            success: true,
            data: getSchedule
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            data: []
        })
    }
}
const addStudent = async(req,res,next) => {
    const {idUser,idClass,id} = req.body
    const addStudentNew = new Student({userId: idUser,classId: idClass})
    try {
        const foundStudent = await Student.findOne({userId: idUser,classId: idClass})
        if(foundStudent){
            return res.status(403).json({
                success: false,
                message: 'found'
            })
        }
        await addStudentNew.save()
        logger.info(`${id} add user to class ${idClass}`)
        return res.status(200).json({
            success: true
        })
    } catch (error) {
        logger.info(`${id} no exitis add user to class ${idClass}`)
        return res.status(500).json({
            success: false
        })
    }
}
const addSchedule = async(req, res,next) => {
    const {id, idClass,date,time,term,title}  = req.body

    const newSchedule = new ScheduleModel({classId: idClass,date,time,term,title})
    console.log(newSchedule)
    try {
        const foundSchedule = await ScheduleModel.findOne({classId:idClass,date,time})
        if(foundSchedule) {
            return res.status(403).json({
                success: false,
                message: "found"
            }) 
        }
        await newSchedule.save()
        return res.status(200).json({
            success: true
        })
    } catch (error) {
        return res.status(500).json({
            success: false
        })
    }

    
}
module.exports = {
    create: CreateNew,
    schedule: getAllSchedule,
    addStudent,
    addSchedule
}