const httpStatus = require('http-status');

const config = require('../configs/config')
const IError = require('../utils/IError')
const logger = require('../configs/logger')
//models
const User = require('../models/User.models')
const QRCode = require('qrcode')
const CryptoJS = require("crypto-js");
const getMyQR = async(req, res,next) => {
    const {qr} = req.query
    console.log(qr)
    var bytes  = CryptoJS.AES.decrypt(qr, config.secret_aes);
    var plaintext = bytes.toString(CryptoJS.enc.Utf8);

    console.log(plaintext)
    
    return res.status(200).json({
        id: plaintext,
        success: true
    })
}
const generalMyQR = async(req, res,next) => {
    const {id} = req.body
    let urlArr = []
   try {
        const uri = await QRCode.toDataURL(id)
        
        const result = {
            url: uri
        }
        urlArr.push(result)
        return res.status(200).json({
            data: urlArr ,
            success: false
        })
   } catch (error) {
        return res.status(500).json({
            url: '',
            success: false
        })
   }
}
module.exports = {
    myQR: getMyQR,
    generalMyQR
}