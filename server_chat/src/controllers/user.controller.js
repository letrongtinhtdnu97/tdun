const httpStatus = require('http-status');

const { jwtExpirationInterval } = require('../configs/config')
const moment = require('moment-timezone');
const { omit } = require('lodash');
const IError = require('../utils/IError')
const logger = require('../configs/logger')
//models
const User = require('../models/User.models')
const RefreshToken = require('../models/RefreshToken.model')
const PasswordResetToken = require('../models/PasswordResetToken.model')

const getAll = async(req,res,next) => {
    try {
        const user = await User.find({})
        if(user.length === 0) {
            return res.status(httpStatus.NOT_FOUND).json({
                data: user,
                message: 'Not found'
            })
        }
        return res.status(httpStatus.OK).json({
            data: user,
            success: true,
            message: 'Get all user success'

        })
    } catch (error) {
        next(error)
    }
}

module.exports = {
    getAll
}