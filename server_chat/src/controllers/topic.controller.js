
const IError = require('../utils/IError')
const logger = require('../configs/logger')
//models
const User = require('../models/User.models')
const Topic = require('../models/Topic.model')
const Images = require('../models/Image.model')
const Comment = require('../models/Comment.model')
const Replies = require('../models/Reply.model')
const create = async(req,res,next) => {
    const {userId,title, emoji, address, flag} = req.body
    const fields = {
        userId,
        title,
        emoji,
        address,
        flag
    }
    const newTopic = new Topic(fields)
    const topicId = newTopic._id
    
    let arrImage = []
   
    req.files.forEach((element) => {
        var count = 0;
        let newImage = new Images({userId,topicId,name: element.filename})
        arrImage.push(newImage._id)
        newImage.save().then(() => arrImage.push(newImage._id)).catch((e) => console.log(e))
        
    });
    

    try {
    

    
        newTopic.imagesId = arrImage
        await newTopic.save()

        return res.status(200).json({
            success: true
        })
    } catch (error) {
        return res.status(500).json({
            success: false
        })
    }

   
}

const userCommentTopic = async(req,res,next) => {
    const {userCommentId,topicId, content} = req.body    
    const cmtNew = new Comment({userCommentId:userCommentId ,topicId: topicId,content})
    try {
        await cmtNew.save()
        return res.status(200).json({
            success: true
        })
    } catch (error) {
        return res.status(500).json({
            success: false
        })
    }

}

const getReplyComment = async(req,res,next) => {
    console.log(req.body)

    const newReplies = new Replies({})
    console.log(newReplies)
    
    return res.status(200).json({
        success: true
    })
}
module.exports = {
    create,
    userCommentTopic,
    getReplyComment
}