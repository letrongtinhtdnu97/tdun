const httpStatus = require('http-status');

const { jwtExpirationInterval } = require('../configs/config')
const moment = require('moment-timezone');
const { omit } = require('lodash');
const IError = require('../utils/IError')
const logger = require('../configs/logger')
//models
const User = require('../models/User.models')
const RefreshToken = require('../models/RefreshToken.model')
const PasswordResetToken = require('../models/PasswordResetToken.model')
function generateTokenResponse(user, accessToken) {
    const tokenType = 'Bearer';
    const refreshToken = RefreshToken.generate(user).token;
    const expiresIn = moment().add(jwtExpirationInterval, 'minutes');
    return {
      tokenType,
      accessToken,
      refreshToken,
      expiresIn,
    };
}

const register = async(req, res, next) => {
    try {
        const userData = omit(req.body, 'role');
        const user = await new User(userData).save();
        const userTransformed = user.transform();
        const token = generateTokenResponse(user, user.token());
        logger.info(`Create new account success [${JSON.stringify(user)}]`)
        return res.status(httpStatus.CREATED).json({ token, user: userTransformed });
    } catch (error) {
        logger.error(`No create account [${JSON.stringify(req.body)}]`)
        return next(User.checkDuplicateEmail(error));
    }
}

const login = async(req, res, next) => {
    try {
        const { user, accessToken } = await User.findAndGenerateToken(req.body);
        const token = generateTokenResponse(user, accessToken);
        const userTransformed = user.transform();
        logger.info(`Account [${JSON.stringify(user)}] login successful`)
        console.log('userTransformed',userTransformed)
        return res.json({ token, user: userTransformed });
      } catch (error) {
        logger.error(`Account [${JSON.stringify(req.body)}] login fail`)
        return next(error);
      }
}

const oAuth = async (req, res, next) => {
    try {
      const { user } = req;
      const accessToken = user.token();
      const token = generateTokenResponse(user, accessToken);
      const userTransformed = user.transform();
      return res.json({ token, user: userTransformed });
    } catch (error) {
      return next(error);
    }
};

const refresh = async (req, res, next) => {
    try {
      const { email, refreshToken } = req.body;
      const refreshObject = await RefreshToken.findOneAndRemove({
        userEmail: email,
        token: refreshToken,
      });
      const { user, accessToken } = await User.findAndGenerateToken({ email, refreshObject });
      const response = generateTokenResponse(user, accessToken);
      return res.json(response);
    } catch (error) {
      return next(error);
    }
};
const sendPasswordReset = async (req, res, next) => {
    try {
      const { email } = req.body;
      const user = await User.findOne({ email }).exec();
  
      if (user) {
        const passwordResetObj = await PasswordResetToken.generate(user);
        emailProvider.sendPasswordReset(passwordResetObj);
        res.status(httpStatus.OK);
        return res.json('success');
      }
      throw new IError({
        status: httpStatus.BAD_REQUEST,
        message: 'No account found with that email',
      });
    } catch (error) {
      return next(error);
    }
};

const resetPassword = async (req, res, next) => {
    try {
      const { email, password, resetToken } = req.body;
      const resetTokenObject = await PasswordResetToken.findOneAndRemove({
        userEmail: email,
        resetToken,
      });
  
      const err = {
        status: httpStatus.BAD_REQUEST,
        isPublic: true,
      };
      if (!resetTokenObject) {
        err.message = 'Cannot find matching reset token';
        throw new IError(err);
      }
      if (moment().isAfter(resetTokenObject.expires)) {
        err.message = 'Reset token is expired';
        throw new IError(err);
      }
  
      const user = await User.findOne({ email: resetTokenObject.userEmail }).exec();
      user.password = password;
      await user.save();
      emailProvider.sendPasswordChangeEmail(user);
  
      res.status(httpStatus.OK);
      return res.json('Password Updated');
    } catch (error) {
      return next(error);
    }
};
const updateUser = async (req, res, next) => {
  const {userId, address,currentAddress,nameClass,smart_phone,home_phone,mail,edu_mail,introduce} = req.body
  const fields = {
    address,
    currentAddress,
    class: nameClass,
    smart_phone,
    home_phone,
    mail,
    edu_mail,
    introduce

  }
  try {
    const user = await User.findOneAndUpdate({_id: userId},fields,{new: true})
    return res.status(200).json({
      data: user,
      success: true
    })
  } catch (error) {
    return res.status(500).json({
      data: [],
      success: false
    })
  }
};
const getUserById = async(req,res,next) => {
  const {userId} = req.body
  try {
    const user = await User.findOne({_id: userId})
    return res.status(200).json({
      data: user,
      success: true
    })
  } catch (error) {
      return res.status(500).json({
        data: [],
        success: false
      })
  }
}
const getAvatar = async(req, res, next ) => {
  console.log(req.body)
  const {userId} = req.body
  try {
    const user = await User.findOne({_id: userId},{}).select('picture');
    
    return res.status(200).json({
      data: user,
      success: true
    })
  } catch (error) {
      return res.status(500).json({
        data: [],
        success: false
      })
  }
}
module.exports = {
    register,
    login,
    oAuth,
    refresh,
    sendPasswordReset,
    resetPassword,
    update: updateUser,
    getUserById,
    getAvatar
}