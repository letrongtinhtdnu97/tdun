const httpStatus = require('http-status');

const { jwtExpirationInterval } = require('../configs/config')
const moment = require('moment-timezone');
const { omit } = require('lodash');
const IError = require('../utils/IError')
const logger = require('../configs/logger')
//models
const Status = require('../models/Status.model')
const Message = require('../models/Message.model')
const Image = require('../models/Image.model')
const RefreshToken = require('../models/RefreshToken.model')
const PasswordResetToken = require('../models/PasswordResetToken.model')

const getAll = async(req,res,next) => {
    console.log(req.body)
    const {page, total} = req.body
    if(!page || !total) {
        return res.status(403).json({
            data: []
        })
    }
    const skip = page === '1' ? 0 : (page-1)*total 
    console.log(skip, total)
    const status = await Status.find()
        .sort({'createAt': -1})
        .limit(Number(total))
        .skip(Number(skip))
        .populate({ path: 'imagesId', select: 'name' })
        .populate({path: 'userId', select: 'lastname firstname'})
    return res.status(200).json({
        data: status
    })
}
const getById = async(req,res,next) => {
    //console.log()
    const {id}  = req.params
    if(!id){
        return res.status(403).json({
            success: false,
            message: 'not id'
        })
    }
    try {
        const getStatus = await Message
        .findOne({statusId: id})
        .populate({path: 'statusId'})
        .populate({path: 'userId', select: 'lastname firstname'})
        const getImage = await Image.find({_id: getStatus.statusId.imagesId[0] })
        .populate({path: 'statusId', select: 'name -_id'})
        return res.status(200).json({
            success: true,
            data: getStatus,
            image: getImage
        })
    } catch (error) {
        return res.status(500).json({
            success: false
        })
    }
}
module.exports = {
    getAll,
    getById
}