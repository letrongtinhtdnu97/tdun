const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const ScheduleSchema = new mongoose.Schema({
  
    classId: {
        type: String
    },
    date: {
        type: Date
    },
    time: {
        type: String
    },
    term: {
        type: String
    },
    title: {
        type: String
    }
});


const ScheduleModel = mongoose.model('Schedules', ScheduleSchema);
module.exports = ScheduleModel;
