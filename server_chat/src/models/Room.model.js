const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const RoomSchema = new mongoose.Schema({
  
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    name: {
        type: String
    },
    createAt: { type: Date, default: Date.now },
  
});


const Images = mongoose.model('rooms', RoomSchema);
module.exports = Images;
