const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const ConversationSchema = new mongoose.Schema({
  
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    statusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Status',
        required: true,
    },
    comment: {
        type: String
    },
    
    createAt: { type: Date, default: Date.now },
  
});


const Conversation = mongoose.model('conversation', ConversationSchema);
module.exports = Conversation;
