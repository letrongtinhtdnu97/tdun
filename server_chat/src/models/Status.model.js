const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const SatatusSchema = new mongoose.Schema({
  
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
      type: String
  },
  imagesId: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Images',
    required: true,
  }],
  emoji: {
    type: Number
  },
  address: {
    type: String
  },
  createAt: { type: Date, default: Date.now() },
  flag: {
      type: Number
  }
});


const Status = mongoose.model('Status', SatatusSchema);
module.exports = Status;
