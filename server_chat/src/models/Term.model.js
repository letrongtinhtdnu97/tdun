const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const TermSchema = new mongoose.Schema({
  
    
    
    name: {
        type: String
    },
    teacherId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    createAt: { type: Date, default: Date.now },
    endAt: {type: Date}
  
});


const Term = mongoose.model('Terms', TermSchema);
module.exports = Term;
