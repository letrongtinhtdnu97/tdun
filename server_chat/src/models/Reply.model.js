const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const ReplySchema = new mongoose.Schema({
  
    
    
    reply: {
        type: String
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    commentId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comments',
        required: true,
    },
    createAt: { type: Date, default: Date.now() },
  
});


const Replies = mongoose.model('replies', ReplySchema);
module.exports = Replies;
