const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const MessageSchema = new mongoose.Schema({
  
    userId: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    }],
    statusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Status',
        required: true,
    },
    text: {
        type: String
    },
    createAt: { type: Date, default: Date.now },
  
});


const Message = mongoose.model('Messages', MessageSchema);
module.exports = Message;
