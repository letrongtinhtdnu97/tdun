const mongoose = require('mongoose');


/**
 * Refresh Token Schema
 * @private
 */
const CommentSchema = new mongoose.Schema({
  
    
    content: {
        type: String
    },
    topicId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Topics',
        required: true,
    },
    userCommentId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    replyId: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'replies',
    }],
    createAt: { type: Date, default: Date.now },
  
});


const Comment = mongoose.model('Comments', CommentSchema);
module.exports = Comment;
