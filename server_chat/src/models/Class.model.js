const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const ClassesSchema = new mongoose.Schema({
  
    name: {
        type: String
    },
    subName: {
        type: String
    },
    startAt: {
        type: Date
    },
    endAt: {
        type: Date
    }
    
});


const Classes = mongoose.model('Classes', ClassesSchema);
module.exports = Classes;
