const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const ImageSchema = new mongoose.Schema({
  
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    statusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Status',
    },
    name: {
        type: String
    },
    topicId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Topics',
    },
    typeName: {
        type: Number, // 1. Status, 1.Avatar
        default: 1
    },
    createAt: { type: Date, default: Date.now },
  
});


const Images = mongoose.model('Images', ImageSchema);
module.exports = Images;
