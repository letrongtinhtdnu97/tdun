const mongoose = require('mongoose');
const crypto = require('crypto');
const moment = require('moment-timezone');

/**
 * Refresh Token Schema
 * @private
 */
const StudentSchema = new mongoose.Schema({
  
  userId: {
    type: String
  },
  classId: {
    type: String
  },
  createAt: {
      type: Date,
      default: Date.now()
  }
});


const Student = mongoose.model('Students', StudentSchema);
module.exports = Student;
