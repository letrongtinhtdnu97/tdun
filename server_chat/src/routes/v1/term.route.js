const express = require('express');
const { validate } = require('express-validation')
const controller = require('../../controllers/term.controller')
const {register} = require('../../validations/auth.validation')

const router = express.Router();


router.route('/create')
  
  .post(controller.create);




module.exports = router;