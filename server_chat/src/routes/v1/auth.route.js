const express = require('express');
const { validate } = require('express-validation')
const controller = require('../../controllers/auth.controller')
const {register, login, refresh, sendPasswordReset, passwordReset, oAuth } = require('../../validations/auth.validation')
const oAuthLogin = require('../../middlewares/auth').oAuth
const router = express.Router();


router.route('/register')
  // Đăng kí 
  .post(validate(register),controller.register);

router.route('/login')
  // Login
  .post(validate(login),controller.login);

router.route('/update-user')
 
  .post(controller.update);

router.route('/get-user')
 
  .post(controller.getUserById);
//get avatar
router.route('/get-avatar')
 
  .post(controller.getAvatar);

router.route('/refresh-token')
  // 
  .post(validate(refresh),controller.refresh)

router.route('/send-password-reset')
  // 
  .post(validate(sendPasswordReset), controller.sendPasswordReset);

router.route('/reset-password')
  .post(validate(passwordReset), controller.resetPassword);

router.route('/facebook')
  .post(validate(oAuth), oAuthLogin('facebook'), controller.oAuth);

router.route('/google')
  .post(validate(oAuth), oAuthLogin('google'), controller.oAuth);
module.exports = router;