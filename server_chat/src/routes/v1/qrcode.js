const express = require('express');
const { validate } = require('express-validation')
const controller = require('../../controllers/qrcode.controller')
const {register} = require('../../validations/auth.validation')

const router = express.Router();


router.route('/myqr')
  
  .get(controller.myQR);

router.route('/myqr')
  // Đăng kí 
  .post(controller.generalMyQR);


module.exports = router;