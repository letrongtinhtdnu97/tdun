const express = require('express');
const { validate } = require('express-validation')
const controller = require('../../controllers/classes.controller')
const {register, login, refresh, sendPasswordReset, passwordReset, oAuth } = require('../../validations/auth.validation')

const router = express.Router();


router.route('/create')
  // Đăng kí 
  .post(controller.create);

router.route('/schedule')
    .post(controller.schedule)
router.route('/add-student')
    .post(controller.addStudent)
router.route('/add-schedule')
    .post(controller.addSchedule)
module.exports = router;