const express = require('express');
const { validate } = require('express-validation')
const controller = require('../../controllers/topic.controller')


const router = express.Router();
const multer = require('multer')
let diskStorage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, "uploads/");
    },
    filename: (req, file, cb) => {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, uniqueSuffix+'.jpg')
    }
});
let uploadFile = multer({storage: diskStorage});
router.route('/create')
  
  .post(uploadFile.array('photos'),controller.create);

router.route('/comment')
  .post(controller.userCommentTopic)
router.route('/reply')
  .post(controller.getReplyComment)

module.exports = router;