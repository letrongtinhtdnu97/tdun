const express = require('express');
//
const authRoutes = require('./auth.route')
const path =require('path')
const router = express.Router();
const multer  = require('multer')
const Status = require('../../models/Status.model')
const Image = require('../../models/Image.model')
const Message = require('../../models/Message.model')
const User = require('../../models/User.models')
const statusController = require('../../controllers/status.controller')

let diskStorage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, "uploads/");
    },
    filename: (req, file, cb) => {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      cb(null, uniqueSuffix+'.jpg')
    }
  });
  
let uploadFile = multer({storage: diskStorage});
router.post("/upload1",uploadFile.array('file'), async(req, res,next) => {
    
    const {userId,title,emoji,address,flag} = req.body
    
    const newStatus = new Status({userId,title,emoji,address,flag})
    
    let newImage
    
    if(req.files.length !== 0){
        newImage = new Image({userId,statusId: newStatus._id, name: req.files[0].filename})
        newStatus.imagesId = newImage._id
    }
    console.log('req.body',req.body)
    console.log('file', req.files)
    
    try {
        if(req.files.length !== 0){
            await newImage.save()
        }
        await newStatus.save()
        const newMessage = new Message({statusId:newStatus._id })
        await newMessage.save()
        return  res.status(200).json({
            success: true
        })
    } catch (error) {
        return res.status(500).json({
            success: false
        })
    }
});
router.post('/create-avatar',uploadFile.array('avatar'), async(req,res,next)=>{
    const { userId } = req.body
    const newPhoto = new Image({userId: userId, typeName: 2, name: req.files[0].filename})
    console.log(newPhoto)
    try {
        await newPhoto.save()
        
        const avatarUser = await User.findOneAndUpdate({_id: userId},{picture: newPhoto.name},{new: true});
        
        if(avatarUser.length === 0){
            return res.status(403).json({
                success: false
            })
        }
        console.log('dsa')
        return res.status(200).json({
            success: true
        })
    } catch (error) {
        return res.status(500).json({
            success: false
        })
    }

})

router.post('/all',statusController.getAll)
router.post('/get/:id',statusController.getById)

module.exports = router;