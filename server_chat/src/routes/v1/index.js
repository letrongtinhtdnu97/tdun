const express = require('express');
//
const authRoutes = require('./auth.route')
const path =require('path')
const router = express.Router();
const statusRoutes = require('./status.route')
const qrcodeRoutes = require('./qrcode')
const termRoutes= require('./term.route')
const classesRoutes = require('./classes.route')
const topicRoutes = require('./topic.route')

router.get('/status', (req, res) => res.send('OK'));

router.use('/auth', authRoutes);
router.use('/status', statusRoutes)
router.use('/qrcode',qrcodeRoutes)
router.use('/profile',termRoutes)
router.use('/class',classesRoutes)
router.use('/topic',topicRoutes)
module.exports = router;