Promise = require('bluebird');
const {port} = require('./configs/config')
const server = require('./configs/express')
const logger = require('./configs/logger')
const mongoose = require('./configs/mongoose')

mongoose.connect();

server.listen(port, ()=> logger.info(`Server started on port ${port}`))
module.exports = server;