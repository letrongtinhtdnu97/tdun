const mongoose = require('mongoose')
const logger = require('./logger')
const {mongo, env} = require('./config')

mongoose.Promise = Promise
mongoose.connection.on('error', (err) => {
    logger.error(`MongoDB connection error: ${err}`);
    process.exit(-1);
});
if (env === 'development') {
    mongoose.set('debug', false);
}
exports.connect = () => {
    mongoose
      .connect(mongo.uri, {
        useCreateIndex: true,
        keepAlive: 1,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
      })
      .then(() => logger.info('mongoDB connected...'));
    return mongoose.connection;
};