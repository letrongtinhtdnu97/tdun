const winston = require('winston');
const fs  = require('fs')
const path = require('path')
const {logsDir} = require('../configs/config')
if (!fs.existsSync(logsDir)) {
    fs.mkdirSync(logsDir);
}
const currentDate = new Date().toJSON().slice(0, 10).replace(/-/g, '-');
const myFormat = winston.format.printf(info => {
    return `${info.timestamp} ${info.level}: ${info.message}`;
  });

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.File({ filename: `${logsDir}/error-${currentDate}.log`, level: 'error' }),
    new winston.transports.File({ filename: `${logsDir}/combined-${currentDate}.log` }),
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
          }),
        myFormat
    ),
  }));
}

logger.stream = {
  write: (message) => {
    logger.info(message.trim());
  },
};

module.exports = logger;
