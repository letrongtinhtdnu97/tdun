const path = require('path')

require('dotenv').config()

module.exports = {
    env: process.env.NODE_ENV,
    port: process.env.PORT || 3003,
    logs: process.env.NODE_ENV === "production" ? "combined" : "dev",
    logsDir: process.env.LOGS_DIR || 'logs',
    mongo: {
        uri: process.env.NODE_ENV === 'test' ? process.env.MONGO_URI_TEST : process.env.MONGO_URI_DEV
    },
    key: process.env.KEY_SECRET || '123321123',
    jwtExpirationInterval: process.env.JWT_EXPIRATION_MINUTES,
    emailConfig: {
        host: process.env.EMAIL_HOST,
        port: process.env.EMAIL_PORT,
        username: process.env.EMAIL_USERNAME,
        password: process.env.EMAIL_PASSWORD,
    },
    secret_aes: process.env.KEY
}
