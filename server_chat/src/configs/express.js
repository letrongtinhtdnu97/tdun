const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const compress = require('compression');
const http = require('http');
const path = require('path')
const cors = require('cors');
const helmet = require('helmet');
const passport = require('passport');
const { logs } = require('./config');
const error = require('../middlewares/error')
const routes = require('../routes/v1')
const routes_v2 = require('../routes/v2')
const strategies = require('../configs/passport')
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../docs/swagger.json')

//set up
const app = express()
app.use("/images", express.static(path.join(__dirname, "../../uploads")));
app.use("/files", express.static(path.join(__dirname, "../../uploads")));
let server = http.createServer(app);

//logs
app.use(morgan(logs));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// middleware
// gzip compression
app.use(compress());
app.use(helmet());
app.use(cors());
app.use(passport.initialize());
passport.use('jwt', strategies.jwt);
//routes
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/v1',routes);
app.use('/v2',routes_v2);


//support error
app.use(error.converter)
app.use(error.notFound)
app.use(error.handler)

module.exports = server;