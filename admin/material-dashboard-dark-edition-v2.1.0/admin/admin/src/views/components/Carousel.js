
import React from 'react';
import {Carousel as Carousel1} from 'react-bootstrap'
const DATA = [
    {
        id: '1',
        title: 'First',
        content: 'Nulla vitae elit libero, a pharetra augue mollis interdum.',
        image: 'http://placehold.it/800x100'
    },
    {
        id: '2',
        title: 'Two',
        content: 'Nulla vitae elit libero, a pharetra augue mollis interdum.',
        image: 'http://placehold.it/800x100'
    },
    {
        id: '3',
        title: 'Three',
        content: 'Nulla vitae elit libero, a pharetra augue mollis interdum.',
        image: 'http://placehold.it/800x100'
    },
    
]
 
const Carousel = (props) => {
    const {data} = props
    
    const renderItem = (item) => {
        return (
            <Carousel1.Item  key={item.id}>
                <img
                className="d-block w-100"
                src={item.image}
                alt="First slide"
                />
                <Carousel1.Caption>
                    <h3>{item.title}</h3>
                    <p>{item.content}</p>
                </Carousel1.Caption>
            </Carousel1.Item>
        )
    }
    return (
        <Carousel1>
            {
                data ? data.map(item => renderItem(item)) : DATA.map(item => renderItem(item) )
            }
            
        </Carousel1>
    );
};

export default Carousel;