import React from 'react';
import {Button as Button1} from 'react-bootstrap'
const Button = (props) => {
    const {name, outline, style,onClick, type, size, disabled} = props
    return (
        <Button1 
            type={type} 
            onClick={onClick} 
            style={style ? style : {}} 
            variant={outline}
            size={size}
            disabled={disabled ? disabled : false}
        >
            {name}
        </Button1>
    );
};

export default Button;