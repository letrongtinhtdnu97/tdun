import React from 'react';
import {Card,ListGroup, Container, Row, Col, Image} from 'react-bootstrap'
const MenuHeader = () => {
    return (
        <Card style={{ paddingTop:10}}>
            <div style={{padding: 20}}>
                    <Row>
                        <Col sm={4} >
                        <ListGroup>
                            <ListGroup.Item>Course 1</ListGroup.Item>
                            <ListGroup.Item>Course 2</ListGroup.Item>
                            <ListGroup.Item>Course 3</ListGroup.Item>
                            <ListGroup.Item>Course 4</ListGroup.Item>
                            <ListGroup.Item>Course 5</ListGroup.Item>
                        </ListGroup>
                        </Col>
                        <Col sm={8}>
                        <Container>
                            <Image src="http://placehold.it/685px250" bsPrefix />
                        </Container>
                        </Col>
                    </Row>
            </div>
                
        </Card>
    );
};

export default MenuHeader;