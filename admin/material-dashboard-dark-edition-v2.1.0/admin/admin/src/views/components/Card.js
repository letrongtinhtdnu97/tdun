import React from 'react';
import {Card as Card1} from 'react-bootstrap'
import Button from './Button'
const Card = (props) => {
    const {title, text, } = props
    return (
        <Card1 style={{ width: '18rem' }}>
            <Card1.Img variant="top" src="holder.js/100px180" />
            <Card1.Body>
                <Card1.Title>Card Title</Card1.Title>
                <Card1.Text>
                Some quick example text to build on the card title and make up the bulk of
                the card's content.
                </Card1.Text>
                <Button name="Go" outline="primary" />
            </Card1.Body>
        </Card1>
    );
};

export default Card;