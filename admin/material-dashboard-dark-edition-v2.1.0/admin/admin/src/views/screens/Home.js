import React from 'react';
import Navigation from '../components/Navigation'
import {Container,Col ,Row} from 'react-bootstrap'
import Carousel from '../components/Carousel'
import MenuHeader from '../components/MenuHeader'
const DATA = [
    {
        id: '1',
        title: 'First',
        content: 'Nulla vitae elit libero, a pharetra augue mollis interdum.',
        image: 'http://placehold.it/800x350'
    },
    {
        id: '2',
        title: 'Two',
        content: 'Nulla vitae elit libero, a pharetra augue mollis interdum.',
        image: 'http://placehold.it/800x350'
    },
    {
        id: '3',
        title: 'Three',
        content: 'Nulla vitae elit libero, a pharetra augue mollis interdum.',
        image: 'http://placehold.it/800x350'
    },
    
]
const Home = (props) => {
    return (
        <>
            <Navigation />
            <Container fluid>
                
            <Container>   
                <MenuHeader />
                <Carousel />

                <Carousel />
                <Carousel />
                <Carousel />
                <Carousel />
                <Carousel />
                <Carousel />
            </Container>
            </Container>
        </>
        
    );
};

export default Home;