import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  products: any;
  users: any;
  constructor(private service: LoginService, private http: HttpClient) {
  }

  ngOnInit(): void {
    this.service.getUser().subscribe((data) => this.users = data);
  }

}
