import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  products: any;

  constructor() {
    this.products = [
      {
        id: '1',
        name: 'watch',
        brand: 'LG',
        price: 700
      },
      {
        id: '2',
        name: 'watch2',
        brand: 'LG2',
        price: 700
      },
      {
        id: '3',
        name: 'watch3',
        brand: 'LG3',
        price: 500
      },
    ];
    }

  ngOnInit(): void {
  }

}
