import { Request, Response, NextFunction, Router} from 'express'
import User from '../models/User'
class UserController {
    router: Router;
    constructor() {
        this.router = Router();
        this.routes();
    }

    async createUser (req: Request, res: Response, next: NextFunction) {
        console.log(req.body)
        const {title, url, content, image} = req.body;
        const newPost = new User({})
        try {
            await newPost.save();
            return res.status(200).json({
                success: true,
                data: newPost
            })
        } catch (error) {
            return res.status(500).json({
                success: false,
            })
        }
    }

    async getUser(req: Request, res: Response) {
        const {id} = req.params

        const user = await User.findOne({_id: id})
        res.json(user)
        //res.json(posts);
    }

    async getUsers(req: Request, res: Response, next: NextFunction){
        try {
            const users =  await User.find();
            return res.json({
                users,
                success: true
            });
        } catch (error) {
            return res.json({
                success: false
            });
        }
    }

    async updateUser(req: Request, res: Response) {
        const {id} = req.params
        const postUpdate = await User.findOneAndUpdate({_id: id}, req.body, {new: true})
        res.json(postUpdate);
    }

    async deleteUser(req: Request, res: Response) {
        const { id } = req.params
        try {
            await User.findOneAndDelete

            return res.status(200).json({
                success: true
            })
        } catch (error) {
            return res.status(500).json({
                success: false
            })
        }
    }
    routes() {
        this.router.get('/', this.getUsers)
        this.router.get('/:id', this.getUser)
        this.router.post('/', this.createUser)
        this.router.put('/:id', this.updateUser)
        this.router.delete('/:id', this.deleteUser)
    }
}

const userController =  new UserController();
export default userController.router;