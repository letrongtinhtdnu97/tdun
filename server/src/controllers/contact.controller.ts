import { Request, Response, NextFunction, Router} from 'express'
import User from '../models/User'


class ContactController {
    router: Router;
    constructor() {
        this.router = Router();
        this.routes();
    }

    getAllContact(req: Request, res: Response, next: NextFunction) {
        return res.status(200).json({
            success: true
        })
    }


    routes() {
        this.router.get('/', this.getAllContact)
    }
}

const contactController = new ContactController()
export default contactController.router;