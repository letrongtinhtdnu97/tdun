import {Router} from 'express'

// controllers
import userController from './controllers/user.controller'
import contactController from './controllers/contact.controller'
//middlewares

const router: Router = Router();


router.use('/user',[userController]);
router.use('/contact',contactController)
export default router;
