import express from 'express';
import morgan from 'morgan'
import helmet from 'helmet'
import mongoose from 'mongoose'
import compression from 'compression'
import cors from 'cors'


//configs 

import configs from './configs'

//middlewares
import nodeErrorHandler from './middlewares/nodeErrorHandler.middleware'



import routers from './routes'
export class Server {
    
    public app: express.Application;    
    configs = configs
    constructor() {
        this.app = express()
        this.config()
        this.routes()
    }

    config() {
        const MONGO_URI = configs.MONGO;
        mongoose.set('useFindAndModify', true)
        mongoose.connect(`${MONGO_URI}`,{
            useNewUrlParser: true,
            useCreateIndex: true,
            useUnifiedTopology: true
        })
        .then(db => console.log('DB connected'))
        this.app.set('port', process.env.PORT || 3000)

        //middlewares
        this.app.use(morgan('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: true}));
        this.app.use(helmet());
        this.app.use(compression());
        this.app.use(cors());
    }

    routes() {
        this.app.use('/api',routers)
    }
    start(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.app.listen(+this.app.get('port'),() => {
                console.log(' Server on port', this.app.get('port'));
                resolve(true)
            })
            .on('err',nodeErrorHandler);
        })
    }
}

// const server = new Server()

// server.start();