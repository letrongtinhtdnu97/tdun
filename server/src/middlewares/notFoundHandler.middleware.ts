import { Request, Response, NextFunction  } from 'express'

export default function notFoundError(req: Request, res: Response, next: NextFunction): void {
    res.status(404).json({
      error: {
        code: 404,
        message: 'Error response middleware for 404 not found.'
      }
    });
  }
  