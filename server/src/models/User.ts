import {Schema, model} from 'mongoose'

const UserSchema = new Schema({
    phone: {
        type: Number,
    },
    email: {
        type: String 
    },
    password: {
        type: String
    },
    first_name: {
        type: String
    },
    last_name: {
        type: String
    },
    middle_name: {
        type: String,
    },
    verification_code: {
        type: String,
        max: 6
    },
    is_active: {
        type: Boolean,
        default: false
    },
    is_block: {
        type: Boolean,
        default:false
    },
    create_at: {
        type: Date,
        default: Date.now
    },
    update_at: {
        type: Date
    }
})


export default model('User',UserSchema)