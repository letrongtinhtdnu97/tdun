import * as dotenv from 'dotenv'

dotenv.config();

const isEnvironment = process.env.NODE_ENV === 'development' 
export default {
    MONGO: process.env.MONGO_URI || 3000
}
