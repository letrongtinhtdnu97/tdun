import { Request, Response, NextFunction, Router} from 'express'

class IndexRoute {

    router: Router;
    constructor() {
        this.router = Router();
        this.routes()
    }

    routes() {
        this.router.get('/', (req,res) => {
            res.send('hello 1')
        })
    }
}

const indexRoute = new IndexRoute()
indexRoute.routes();

export default indexRoute.router