import { Request, Response, NextFunction, Router} from 'express'
import Post from '../models/Post'
class PostRoutes {
    router: Router;
    constructor() {
        this.router = Router();
        this.routes();
    }

    async createPost (req: Request, res: Response) : Promise<void> {
        console.log(req.body)
        const {title, url, content, image} = req.body;
        const newPost = new Post({title,url,content,image})
        await newPost.save();
        res.json({
            success: true,
            data: newPost
        })
    }

    async getPost(req: Request, res: Response): Promise<void> {
        const {url} = req.params

        const post = await Post.findOne({url})
        console.log(req.params)
        res.json(post)
        //res.json(posts);
    }

    async getPosts(req: Request, res: Response): Promise<void> {
        const posts =  await Post.find();
        res.json(posts);
    }

    async updatePost(req: Request, res: Response): Promise<void> {
        const {url} = req.params
        console.log(req.body)
        const postUpdate = await Post.findOneAndUpdate({url}, req.body, {new: true})
        console.log(postUpdate)
        res.json(postUpdate);
    }

    async deletePost(req: Request, res: Response): Promise<void> {
        const { id } = req.params
        await Post.findOneAndDelete
    }
    routes() {
        this.router.get('/api/posts', this.getPosts)
        this.router.get('/api/posts/:url', this.getPost)
        this.router.post('/api/posts', this.createPost)
        this.router.put('/api/posts/:url', this.updatePost)
        this.router.delete('/api/posts/:id', this.deletePost)
    }
}

const postRoutes =  new PostRoutes();
export default postRoutes.router;