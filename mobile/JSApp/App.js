
import React, {useState} from 'react';
import { createStore,applyMiddleware  } from 'redux';
import { Provider } from 'react-redux';
import allReducers  from './src/redux/reducers'
import createSagaMiddleware from 'redux-saga';
import NavigationProvider from './src/navigations'
import root from './src/redux/sagas';
const sagaMiddleware = createSagaMiddleware();
let store = createStore(allReducers,applyMiddleware(sagaMiddleware));

const App = () => {
  return(
    <Provider store={store}>
      <NavigationProvider />
      
    </Provider>
  );
}

sagaMiddleware.run(root);

export default App;
