import React,{useState, useEffect} from 'react';
import {StyleSheet, Text, View, ScrollView, Modal, TouchableOpacity,FlatList} from 'react-native'
import Avatar from '../components/Profile/Avatar'
import LinearGradient from 'react-native-linear-gradient';
import {useDispatch, useSelector} from 'react-redux'
import Icon from 'react-native-vector-icons/AntDesign'
import Nav from '../components/Profile/Nav'
import { ListItem } from 'react-native-elements';
import mocks from '../mocks/profile.json'
import axios from 'axios';
import ImageViewer from 'react-native-image-zoom-viewer';
import LoadHub from '../components/LoadHub'
import QrReader from 'react-qr-scanner'
import Footer from '../components/Profile/Footer'
const image = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Dogsledatrest.jpg/1200px-Dogsledatrest.jpg"

const Profile = (props) => {
    const {navigation} = props
    const dispatch = useDispatch()
    const authReducer = useSelector(state => state.authencationReducer)
    const [fullName, setFullName] = useState('')
    const [email, setEmail] = useState('')
    const [data, setData] = useState([])
    const [myQR, setMyQr] = useState([])
    const [showQR, setShowQr] = useState(false)
    const [hide,setHide] = useState(false)
    const [delay, setDelay] = useState(100)
    useEffect(() => {
        setHide(true)
        setData(mocks)
        authReducer?.user ? setFullName(authReducer.user.firstname + ' ' + authReducer.user.lastname) : setFullName('')
        setHide(false)
    },[authReducer])
    const handleOpenCarema = () => {
        
    }
    const handleError = () => {}
    const handleScan = () => {}
    const handleScan1 = () => {
        return(
            <QrReader
                delay={delay}
                style={styles.previewStyle}
                onError={handleError}
                onScan={handleScan}
            />
        );
    }
    const handleMyQR = async() => {
       
        // const key = 'mykey'
        
        // const dataQR =  authReducer.user.id
        // var ciphertext = CryptoJS.AES.encrypt(dataQR, key);
        // console.log(ciphertext.toString())
        setHide(true)
        try {
            const requestClient = {
                id: authReducer.user.id
            }
            const result = await axios.post(`http://192.168.1.19:3003/v1/qrcode/myqr`,requestClient)
            setMyQr(result?.data?.data ? result.data.data: '')
            setShowQr(true)
            setHide(false)
        } catch (error) {
            setShowQr(false)
            setHide(false)
        }
        
    }
    
    const handleClick = (id) => {
        switch (id) {
            case '1':
                handleMyQR()
                break;
            case '2': 
                handleOpenCarema()
                break;
            case '3': 
                handleScan1()
                break;
            default:
                break;
        }
    }
    
    const renderItem = (item) => {
       return(
        <ListItem
            Component={TouchableOpacity}
            onPress={() => handleClick(item.id)}
            friction={90} //
            tension={100} // These props are passed to the parent component (here TouchableScale)
            activeScale={0.95} //
            linearGradientProps={{
                colors: ['#FF9800', '#F44336'],
                start: { x: 1, y: 0 },
                end: { x: 0.2, y: 0 },
            }}
            ViewComponent={LinearGradient} // Only if no expo
            leftIcon={<Icon size={30} name={item.icon} />}
            title={item.name}
            titleStyle={{ color: 'white', fontWeight: 'bold' }}
            subtitleStyle={{ color: 'white' }}
            subtitle={item.sub}
            chevron={{ color: 'white' }}
        />
       );
    }
    return (
        <>
        <Nav navigation={navigation} name={fullName ? fullName : 'Profile'} />
        <FlatList
            style={{paddingHorizontal:10}}
            data={data}
            ListHeaderComponent={() => (
                <>
                    <View style={{height:350}}>
                        <Avatar navigation={navigation} name={fullName} />
                    </View>
                </>
            )}
            ItemSeparatorComponent={() => (
                <View style={{height:4, backgroundColor:'white'}} />
            )}
            renderItem={({ item }) => renderItem(item)}
            keyExtractor={item => item.id}
            ListFooterComponent={() => <Footer navigation={navigation} />}
            />
          
            <Modal visible={showQR} transparent={true}>
                
                <ImageViewer imageUrls={myQR}/>
                <TouchableOpacity style={{position:'absolute', right:10, top:10}} onPress={() => setShowQr(false)}>
                    <Icon  name="close" size={30} color='white' />
                </TouchableOpacity>
            </Modal>
            <LoadHub hide={hide} />
        </>
    );
};

const styles = StyleSheet.create({
    previewStyle: {
        height: 240,
        width: 320,
    }
})
export default Profile;