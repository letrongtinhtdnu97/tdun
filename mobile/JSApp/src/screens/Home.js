import React,{useEffect, useState} from 'react';
import {StyleSheet, Text, View, BackHandler, Image, Dimensions, FlatList} from 'react-native'
import Nav from '../components/Nav'
import Emoji from '../components/Home/Emoji'
import HeaderComment from '../components/Home/HeaderComment'
import Icon from 'react-native-vector-icons/Entypo'
import Status from '../components/Home/Status'
import {useSelector,useDispatch} from 'react-redux'
import {callFetchStatus} from '../redux/actions/home'
import LoadHub from '../components/LoadHub'
import {getAvatar} from '../redux/actions'

const screenHeight = Dimensions.get('window').height


const Home = (props) => {
    const {navigation} = props
    const dispatch = useDispatch()
    const [hide, setHide] = useState(false)
    const [arrData, setArrData] = useState([])
    const authSelector = useSelector(state => state.authencationReducer)
    const homeSelector = useSelector(state => state.homeReducer)
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', () => true)
        return () =>
        BackHandler.removeEventListener('hardwareBackPress', () => true)
      }, [])
    
    useEffect(() => {
        //alert(screenWith)
        const data = {
            page: 1,
            total: 10
        }
        dispatch(callFetchStatus(data))
        const data1 = {
            userId: authSelector.user.id
        }
        dispatch(getAvatar(data1))
    },[])
    
    useEffect(() => {
        setHide(true)
        if(homeSelector.isLoading) {
            homeSelector?.data ? setArrData(homeSelector.data) : setArrData([])
            setHide(false)
        }
    },[homeSelector])
    if(!arrData) {
        return(
            <View>
                <Text>khong co gi ca</Text>
            </View>
        );
    }
    return (
        <View style={styles.container}>
            <Nav heart={true} camera={true} navigation={navigation} />
            <View style={{paddingBottom:50}}>
                <FlatList 
                    data={arrData}
                    renderItem={({item}) => <Status navigation={navigation} data={item} />}
                    keyExtractor={item => item._id}
                />
            </View>
            <LoadHub hide={hide} />
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        width: 100 + "%", 
        height: 100 + "%"
    },
    subContainer: {
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'space-between'
    },
    iconAvatar: {
        //alignSelf:'flex-start'
        paddingRight:25
    },
    menu: {
        alignItems:'flex-end',
    },
    containerContent: {
        paddingVertical:8,
        paddingHorizontal: 8,
        flexDirection: 'row',
        alignItems:'center'
    },
    textName: {
        paddingLeft:8,
        fontSize:16,
        fontWeight:'bold'
    } ,
    image: {
        width: 100 + "%", 
        height: screenHeight/2.3
    },
    wrapper: {
        paddingTop: 5
    },
    avatar: {
        height:40,
        width:40,
        borderRadius:40
    }
})
export default Home;