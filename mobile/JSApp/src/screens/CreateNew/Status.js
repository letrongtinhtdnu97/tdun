import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TextInput, Dimensions, TouchableOpacity,KeyboardAvoidingView} from 'react-native'
import Icon from 'react-native-vector-icons/Entypo'
import ImagePicker from 'react-native-image-crop-picker';
import Button from '../../components/Button'
import {useSelector, useDispatch} from 'react-redux'
import axios from "axios"
import Nav from './Nav'
import LoadHub from '../../components/LoadHub'
import { Tooltip,Overlay,ListItem   } from 'react-native-elements';
import Screens from '../../navigations/Screens'
const screenHeight = Dimensions.get('window').height
const screenWith =Dimensions.get('window').width
const image1 = require('../../assets/default.jpg')
const list = require('../../mocks/address.json')
const emojiIcon = require('../../mocks/emoji.json')
import {smile0,smile1,smile2,smile3,sad} from '../../assets'
const Status = (props) => {
    
    
    const {navigation} = props
    const [text, setText] = useState('')
    const [tag, setTag] = useState('')
    const [image, setImage] = useState(null)
    const [privacy, setPrivacy] = useState(1)
    const [namePrivacy, setNamePrivacy] = useState('')
    const authReducer = useSelector(state => state.authencationReducer)
    const [address, setAddress] = useState('')
    const [emoji, setEmoji] = useState('')
    const [isVisible, setVisible] = useState(false)
    const [isEmojiIcon, setEmojiIcon] = useState(false)
    const [title, setTitle] = useState('')
    const [sImage, setSImage] = useState(null)
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        if(privacy === 1) {
            setNamePrivacy("Công khai")
        }
        if(privacy === 2){setNamePrivacy("Bạn bè")}
        if(privacy === 3){setNamePrivacy("Một mình")}
        
    },[privacy])
    console.log(image)
    const handleCheckIn = (name) => {
        setVisible(false),
        setAddress(name)
    }
    const handleChooseEmoji = (name) => {
        setEmojiIcon(false),
        setEmoji(name)
    }
    const onpeFolder = () => {
        ImagePicker.openPicker({
            multiple: true
          }).then(images => {
            const arrImage = images.map((item) => {
                return {
                    uri: item.path,
                    type: item.mime || 'image/jpg',
                    name: images.filename || Math.floor(Math.random() * Math.floor(999999999)) + '.jpg',
                }
            })
            setSImage(images)
            setImage(arrImage)
        }).catch(err => console.log('err'))
        ;
    }
    const onHandle = async() => {
        setLoading(true)
        let NumberEmoji = 0
        switch (emoji) {
            case 'feeling':
                NumberEmoji = 4
                break;
                case 'sad':
                    NumberEmoji = 3
                break;
                case 'heart':
                    NumberEmoji = 2
                break;
                case 'smile':
                    NumberEmoji = 1
                break;
        
            default:
                NumberEmoji = 0
                break;
        }
        let formData = new FormData()

        sImage.forEach((i) => {
            const file = {
                uri: i.path,
                name: i.filename || Math.floor(Math.random() * Math.floor(999999999)) + '.jpg',
                type: i.mime || 'image/jpeg'
            }
            formData.append('file', file);
        })
        formData.append('userId',authReducer.user.id)
        formData.append('title',title)
        formData.append('emoji',NumberEmoji)
        formData.append('address',address)
        formData.append('flag',privacy)
        console.log(formData)
        axios.post(`http://192.168.1.19:3003/v1/status/upload1`,formData,{
            headers: {"Content-Type": "multipart/form-data"}
        })
        .then( async(rs) => {
            
            if(rs?.data?.success) {
                return navigation.navigate(Screens.HOMEPAGE)
            }  
            
        })
        .catch((e) => console.log(e))
    }
    const handleUpLoad = async() => {
          
            
        
    }
    const TimeOutLoading = () => {
        
        const timer = setTimeout(() => {
            setLoading(false)
        }, 1000);
        return () => clearTimeout(timer);
    }
    const SetUpStatus = () => {
        return(
            <View style={styles.box}>
                <Icon name='ccw' size={12} />
                <Tooltip
                skipAndroidStatusBar={true}
                containerStyle={{height:120, width:120}}
                popover={
                    <View style={{height:100, alignItems:'center', flexDirection:'column', justifyContent:'center'}}>
                        <TouchableOpacity
                        onPress={() => setPrivacy(1)}
                        style={styles.boxPri}>
                            <Text style={{alignSelf:'center', color:'white'}}>Công khai</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                        onPress={() => setPrivacy(2)}
                        style={{height:40, width:120}}>
                            <Text style={{alignSelf:'center', paddingTop:8, color:'white'}}>Bạn bè</Text>
                        </TouchableOpacity>
                        <TouchableOpacity 
                        onPress={() => setPrivacy(3)}
                        style={{height:30, width:120,borderTopWidth:1, borderColor:'white'}}>
                            <Text style={{alignSelf:'center', paddingTop:6, color:'white'}}>Một mình</Text>
                        </TouchableOpacity>
                    </View>
                }>
                    <Text>{namePrivacy}</Text>
                </Tooltip>
                <Icon name='chevron-down' size={12} />
            </View>
            
        );
    }
    const ChooseEmojie = (id) => {
        switch (id) {
            case "1":
                return smile0
            case "2":
                return smile1
            case "3":
                return sad
            case "4":
                return smile3
            default:
                break;
        }
    }
    return (
        <View style={styles.container}>
            
            <KeyboardAvoidingView behavior='position' >
            <Nav onHandle={onHandle} navigation={navigation} />
            <View style={styles.auth}>
                    <View style={styles.viewCover}>
                        <Image resizeMode='cover' style={styles.image} 
                            source={{uri: 'http://192.168.1.19:3003/images/1592389943746-22202375.jpg'}} />
                        <View style={styles.textCover}>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontWeight:'bold', fontSize:16}}>
                                    {authReducer.user && authReducer.user.firstname + " " +authReducer.user.lastname}
                                </Text>
                                {
                                    emoji !== '' ? <Text style={{fontSize:16}}>
                                    {' '}is <Text style={{fontWeight:'bold'}}>{emoji}</Text>
                                 </Text> : null
                                }
                                {address !== '' ? <Text style={{fontSize:16}}>
                                   {' '}is at <Text style={{fontWeight:'bold'}}>{address}</Text>
                                </Text> : null }
                            </View>
                            <SetUpStatus />
                        </View>  
                    </View>
                </View>
                
                <TextInput
                        multiline
                        placeholder={`What's your mind, ${authReducer.user && authReducer.user.firstname}?`}
                        style={{ padding:20 }}
                        onChangeText={text => setTitle(text)}
                        value={title}
                />
                
                
            </KeyboardAvoidingView>
            {
               image ?  <View style={{width:100+'%', height:50+'%'}}>
                   
                   <Image 
                        resizeMode="cover" style={styles.imageContent} 
                        source={{uri: image[0].uri }} 
                    />
                <TouchableOpacity style={{position:'absolute', right:10}} onPress={() => setImage(null)}>
                    <Icon  name='cross' size={30} color='white' />
                </TouchableOpacity>
               </View> : null
            }
            
            <View style={styles.footer}>
                    <View style={styles.coverBoxFooter}>
                    <View style={styles.boxFooter}>
                        <Button onPress={() => onpeFolder()} style={{width:90}}><Text style={{fontSize:14}}>Pictures</Text></Button>
                        <Button style={{width:90}}><Text style={{fontSize:14}}>Friends</Text></Button>
                        <Button onPress={() => setVisible(true)} style={{width:90}}><Text style={{fontSize:14}}>Check in</Text></Button>
                        <Button onPress={() => setEmojiIcon(true)} style={{width:90}}><Text style={{fontSize:14}}>Emoji</Text></Button>
                    </View>
                    </View>
                </View> 
            <Overlay isVisible={isVisible}  overlayStyle={{width:80+"%"}} >
            <View>
            {
                list.map((l, i) => (
                <ListItem
                    onPress={() => handleCheckIn(l.subtitle)}
                    key={i}
                    leftIcon={{name: 'av-timer'}}
                    title={l.name}
                    subtitle={l.subtitle}
                    bottomDivider
                />
                ))
            }
            </View>
            </Overlay> 
            <Overlay isVisible={isEmojiIcon}  overlayStyle={{width:80+"%"}} >
            <View>
            {
                emojiIcon.map((l, i) => (
                <ListItem
                    onPress={() => handleChooseEmoji(l.subtitle)}
                    key={i}
                    leftIcon={<Image style={{width:30, height:30}} source={ChooseEmojie(l.id)} />}
                    title={l.name}
                    subtitle={l.subtitle}
                    bottomDivider
                />
                ))
            }
            </View>
            </Overlay>
            <LoadHub hide={loading} />
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
         backgroundColor:'white'
    },
    boxPri: {height:30, width:120, borderBottomWidth:1, borderColor:'white'},
    auth: {
        height: 80,
        paddingVertical:10,
        paddingHorizontal:10
    },
    viewCover: {
        flexDirection: 'row',
        alignItems:'center',
    },
    imageContent: {width:100+'%', height:100+'%' },
    image: {width:50, height:50, borderRadius:30},
    textCover: {
        paddingHorizontal:8
    },
    box: {
        width: 100,
        height:20,
        borderWidth:1,
        alignItems:'center',
        justifyContent:'space-around',
        flexDirection:'row',
        borderRadius:3
    },
    footer: {
        position:'absolute', 
        bottom:1, 
        width:100+'%',
        height:80,
        padding: 10,
        
    },
    boxFooter: {
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        padding: 10
    },
    coverBoxFooter: {
        borderWidth:1,
        borderRadius:10
    }
    


    
})
export default Status;