import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import {Avatar} from 'react-native-elements'
const Nav = (props) => {
    const {navigation, onHandle} = props
    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Icon style={{paddingLeft:10}} name='close' size={30} />
            </TouchableOpacity>
            <Text style={styles.text}>Tạo bài viết</Text>
            <TouchableOpacity onPress={onHandle}>
                <Text style={[styles.text,{paddingRight:15}]}>Đăng</Text>
            </TouchableOpacity>
        </View>
    );
};
const styles = StyleSheet.create({
   container: {
       width: 100+'%',
       height: 59,
       borderBottomWidth: 1,
       borderColor: '#ccc',
       elevation:1,
       flexDirection:'row',
       justifyContent:'space-between',
       alignItems:'center'
   } ,
   text: {fontSize:20, fontWeight:'bold'}
})
export default Nav;