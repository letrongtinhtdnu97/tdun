import React, { useEffect,useState } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Dimensions,
  FlatList,
  Image
} from 'react-native';

import moment from 'moment'
import {useSelector,useDispatch} from 'react-redux'
import scheduleReducer from '../redux/reducers/schedule';
import {callSchedule} from '../redux/actions'
import {tick} from '../assets'
import LinearGradient from 'react-native-linear-gradient'
const screenHeight = Dimensions.get('window').height
const renderData = (item) => {
    const currentDate = moment(new Date()).format('YYYY-MM-DD')
    const dd = moment(item.date).format('YYYY-MM-DD')
    return(
        <View style={{flexDirection:'column', padding:10}}>
                 
                
            <View style={[styles.containerBox,{ backgroundColor:currentDate > dd ? '#ccc' : 'white'}]}>
                    <View style={{ width:100+'%', height:100+'%', flexDirection:'row'}}>
                        <View style={{width:120, borderRightWidth:1, borderRightColor:'black', alignItems:'center', justifyContent:'center'}}>
                            <Text style={{fontSize:20, fontWeight:'bold'}}>{moment(item.date).format('dddd')}</Text>
                            <Text>{moment(item.date).format('YYYY-MM-DD')}</Text>
                            <Text>({item.time})</Text>
                        </View>
                        <View style={{justifyContent:'space-around', paddingHorizontal: 20}}>
                            <Text>{item.term}</Text>
                            <Text>{item.title}</Text>
                            <Text>Phong 1.02</Text>
                        </View>
                        {
                            currentDate > dd ? 
                            <Image  style={{position:'absolute', width:32, height:32, right:10, bottom:10}} source={tick}/>
                            : null
                        }
                    </View>
            </View>

        </View>
    );
}
const TimeLine = () => {
    const [arrData, setArrData] = useState([])
    const dispath = useDispatch()
    const authReducer = useSelector(state => state.authencationReducer)
    const scheduleSelector = useSelector( state => state.scheduleReducer)
    const [idUser,setIdUser] = useState('')
    useEffect(() =>{
        const rq = {
            id: authReducer.user.id
        }
        dispath(callSchedule(rq))
    },[])
    useEffect(() =>{
        scheduleSelector?.data?.data ? setArrData(scheduleSelector.data.data) : setArrData([])
    },[scheduleSelector])
    console.log('arrData',arrData)
    
    return (
        <View style={{backgroundColor:'#83e7eb', flex:1}}>
            <LinearGradient 
            colors={['#83e7eb','#0db0bf']}
            start={{x:0,y:0}} 
            end={{x:1,y:0}}
            style={{flex:1}}
            >
                <Text style={styles.title}>THỜI KHÓA BIỂU TUẦN</Text>
            <FlatList 
                data={arrData}
                renderItem={({item}) => renderData(item)}
                keyExtractor={item => item._id.toString()}
            /> 
            </LinearGradient>
           
        </View>
        
    );
}
const styles = StyleSheet.create({
    containerBox: {
        
        width:100+'%', 
        height:screenHeight/7, 
        padding:10, 
        elevation:2, 
        borderWidth:1, 
        borderColor:'white', 
        borderRadius:10
    },
    title: {
        alignSelf:'center', paddingVertical:20, fontWeight:'bold', fontSize:25, fontStyle:'italic'
    }
})
export default TimeLine;