import React,{useState, useEffect} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
  } from 'react-native';
import TextGradient from '../components/TextGradient'
import Login from './authencation/Login'
import Register from './authencation/Register'
import {useSelector,useDispatch} from 'react-redux'
import {callReset} from '../redux/actions'
import LoadHub from '../components/LoadHub'
const Authencation = () => {
    const [tab, setTab] = useState(0);
    const [hide, setHide] = useState(false)
    const dispatch = useDispatch();
    const registerSelector = useSelector(state => state.registerReducer)
    useEffect(() => {
      if(registerSelector.isLoading && registerSelector.isSuccess) {
          setHide(true)
          setTab(0)
          dispatch(callReset())
          setHide(false)
      }
      
    },[registerSelector])
    return (
        <SafeAreaView style={styles.container}>
        <TextGradient style={styles.title}>
          {tab == 0 ? 'LOGIN' : 'REGISTER'}
        </TextGradient>
        <View style={styles.listTab}>
          <TouchableOpacity onPress={()=> setTab(0)} style={[styles.button, tab==0 && {borderBottomColor: "#ed719e"}]}>
            <Text style={[styles.buttonText, tab==0 && {color: '#ed719e'}]}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=> setTab(1)} style={[styles.button, tab==1 && {borderBottomColor: "#ed719e"}]}>
            <Text style={[styles.buttonText, tab==1 && {color: '#ed719e'}]}>Create account</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.wrapper}>
          {tab === 0 && (<Login />)}
          {tab === 1 && (<Register />)}
        </View>
        {/* <View style={styles.footer}>
          <Text style={styles.footerText}>OR CREATE ACCOUNT WITH</Text>
          <View style={styles.listButtonAccount}>
            <ButtonIcon icon="twitter" color="#eb63d3" />
            <ButtonIcon icon="google" color="#ee879f" />
            <ButtonIcon icon="facebook-square" color="#ed726e" />
          </View>
        </View> */}
        <LoadHub hide={hide} />
      </SafeAreaView>
    );
};
const styles = StyleSheet.create({
  
    container: {
      flex: 1
    },
    footerText: {
      fontSize:16,
      color:'gray',
      fontWeight:'600',
      textAlign: 'center'
    },
    listButtonAccount: {
      flexDirection:'row',
      justifyContent:'space-around',
      paddingVertical:20
    },
    title:{
      fontSize: 30,
      fontWeight:'800',
      textAlign:'center',
      padding:10,
      fontStyle:'italic',
      paddingTop:20
    },
    listTab: {
      flexDirection:'row'
    },
    button: {
      flex: 1,
      justifyContent:'center',
      alignItems:'center',
      padding: 10,
      borderBottomWidth:3,
      borderBottomColor: '#f4c1ed',
      paddingVertical:13
    },
    buttonText: {
      fontSize:16,
      fontWeight:'600',
      color:'gray'
    },
    footer: {
      paddingVertical:20,
      paddingHorizontal:40
    },
    wrapper: {
      flex: 1,
      paddingHorizontal: 40,
      paddingVertical:30,
    }
});
export default Authencation;