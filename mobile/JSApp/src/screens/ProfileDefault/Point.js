import React from 'react';
import {View, Text, TouchableOpacity,FlatList} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign'
import mocks from '../../mocks/chat.json'
const renderItem = (item) => {
    return(
        <ListItem
            Component={TouchableOpacity}
            onPress={() => handleClick(item.id)}
            friction={90} //
            tension={100} // These props are passed to the parent component (here TouchableScale)
            activeScale={0.95} //
            linearGradientProps={{
                colors: ['#FF9800', '#F44336'],
                start: { x: 1, y: 0 },
                end: { x: 0.2, y: 0 },
            }}
            ViewComponent={LinearGradient} // Only if no expo
            leftIcon={<Icon size={30} name={item.icon} />}
            title={item.name}
            titleStyle={{ color: 'white', fontWeight: 'bold' }}
            subtitleStyle={{ color: 'white' }}
            subtitle={item.sub}
            chevron={{ color: 'white' }}
        />
    );
}
const Point = (props) => {
    return (
        <View>
            <FlatList
            style={{paddingHorizontal:10, marginTop:20}}
            data={mocks}
            
            ItemSeparatorComponent={() => (
                <View style={{height:4, backgroundColor:'white'}} />
            )}
            renderItem={({ item }) => renderItem(item)}
            keyExtractor={item => item.id}
            
            />
        </View>
    );
};

export default Point;