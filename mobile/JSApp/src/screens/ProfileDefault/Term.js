import React,{useState} from 'react';
import {View, Text, StyleSheet, Dimensions,FlatList, TouchableOpacity, Image } from 'react-native'
import mocks from '../../mocks/term.json'
const screensHeight = Dimensions.get('window').height
import {tick, alert} from '../../assets'
const Item = (item) => {
    const gray = `#ccc`
    const date = '2015-2019'
    const currenDate = new Date().getTime()
    const  {countTerm, passTerm, unPassTerm, warning} = item
    return(
        <TouchableOpacity 
            onPress={() => console.log('dsa')} 
            style={[styles.coverContainer,{backgroundColor: warning ? 'red' : '#4287f5'}]}>
                <View style={styles.containerBox}>
                    <View style={styles.box}>
                        <Text style={styles.text}>{item.name}</Text>
                        <Text style={styles.text}>{date}</Text>
                    </View>
                    <View style={styles.boxFooter}>
                        <Text style={styles.text}>Tổng số tín chỉ: 1111</Text>
                        <View>
                            <Text style={[styles.text,styles.paddingLeft]}>Hoàn thành: 111</Text>
                            <Text style={[styles.text,styles.paddingLeft]}>Chưa hoàn thành: 222</Text>
                        </View>
                    </View>
                    
                </View>
                {
                    unPassTerm === 0 ? <Image  
                    resizeMode='contain' 
                    style={{height:60, height:60, position:'absolute', right:10, bottom:5}}  
                    source={tick}/>: null
                }
                {warning ? <Image  
                    resizeMode='contain' 
                    style={{height:60, height:60, position:'absolute', right:80, bottom:5}}  
                    source={alert}/>: null
                }
        </TouchableOpacity>
    );
}
const Term = () => {
    const [color,setColor] = useState('#ccc')
    return (
        <View style={styles.container}>
            <FlatList 
                data={mocks}
                renderItem={({item}) => Item(item)}
                keyExtractor={item => item.id.toString()}
            />
            
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor:'#ccc',
        alignItems:'center'
        
    },
    coverContainer: {
        marginVertical:10,
        marginHorizontal:10,
        width:350,
        height:screensHeight/6,
        //backgroundColor:'#4287f5',
        borderRadius:15,
        elevation:5
    },
    box: {
        paddingVertical:8, 
        paddingHorizontal:8,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    text: {
        color: 'white'
    },
    containerBox: {
        flexDirection:'column'
    },
    boxFooter: {
        paddingLeft:20
    },
    paddingLeft: {
        paddingLeft: 20
    }
})

export default Term;