import React, {useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, ScrollView, TextInput} from 'react-native'
import { Avatar } from 'react-native-elements';
import Button from '../../components/Button'
import {useDispatch, useSelector} from 'react-redux'
import {getProfileUser,updateProfileUser} from '../../redux/actions/profile'
import LoadHub from '../../components/LoadHub'
import {defaultUser,baseUrl} from '../../assets'
import ImagePicker from 'react-native-image-crop-picker';
import axios from 'axios'
const Introduce = (props) => {
    const userSelector = useSelector(state => state.profileReducer)
    const avatarSelector = useSelector(state => state.avatarReducer)
    const authReducer = useSelector(state => state.authencationReducer)
    const dispatch = useDispatch()
    const {userId, route} = props

    const [image , setImage] = useState(null)

    const [isLoading, setLoading] = useState(false)
    const [introduce, setIntroduce] = useState('')
    const [address, setAddress] = useState('')
    const [currentAddress, setCurentAddress] = useState('')
    const [nameClass, setNameClass] = useState('')
    const [sphone, setSphone] = useState('')
    const [cphone, setCphone] = useState('')
    const [mail, setMail] = useState('')
    const [edumail, setEduGmail] = useState('')
    const [simage, setSImage] = useState(null)
    useEffect(() => {
        setLoading(true)
        const data = {
            userId: authReducer.user.id
        }
        dispatch(getProfileUser(data))
    },[])
    
    console.log('avatarSelector',avatarSelector)
    useEffect(() => {
        userSelector?.data?.data?.introduce ? setIntroduce(userSelector.data.data.introduce) : setIntroduce('')
        userSelector?.data?.data?.address ? setAddress(userSelector.data.data.address) : setAddress('')
        userSelector?.data?.data?.currentAddress ? setCurentAddress(userSelector.data.data.currentAddress) : setCurentAddress('')
        userSelector?.data?.data?.class ? setNameClass(userSelector.data.data.class) : setNameClass('')
        userSelector?.data?.data?.smart_phone ? setSphone(userSelector.data.data.smart_phone) : setSphone('')
        userSelector?.data?.data?.home_phone ? setCphone(userSelector.data.data.home_phone) : setCphone('')
        userSelector?.data?.data?.mail ? setMail(userSelector.data.data.mail) : setMail('')
        userSelector?.data?.data?.edu_mail ? setEduGmail(userSelector.data.data.edu_mail) : setEduGmail('')
        if(userSelector?.isLoading) {
            setLoading(false)
        }
    },[userSelector])
    const handleOpenImage = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
          }).then(images => {
            let data = {
                uri: images.path,
                mime: images.mime || 'image/jpeg',
                name: images.filename || Math.floor(Math.random() * Math.floor(999999999)) + '.jpg'
            }
            setImage(data)
            setSImage(images)
          });
    }
    const handleUpdate = async () => {
        const fields = {
            userId: authReducer.user.id,
            address,
            currentAddress,
            nameClass,
            smart_phone: sphone,
            home_phone: cphone,
            mail,
            edu_mail: edumail,
            introduce
        
          }
          dispatch(updateProfileUser(fields))

          let formData = new FormData()
          if(image){
            const file = {
                uri: simage.path,
                name: simage.filename || Math.floor(Math.random() * Math.floor(999999999)) + '.jpg',
                type: simage.mime || 'image/jpeg'
            }
            formData.append('avatar', file);
            formData.append('userId',authReducer.user.id)
            console.log(formData)
            axios.post(`http://192.168.1.19:3003/v1/status/create-avatar`,formData,{
                headers: {"Content-Type":"multipart/form-data"}
            }).then(() => console.log('succs')).catch((e) => console.log(e) )
            
          }
           

    }
    console.log(authReducer?.user?.picture)
    return (
        <>
        <ScrollView style={styles.container}>
           { authReducer?.user?.picture ?
               <View style={styles.avatar}>
               <Avatar
                   rounded
                   activeOpacity={0.7}
                   showAccessory
                   onAccessoryPress={() => handleOpenImage()}
                   size={150}
                   source={{uri: image ? image.uri : `${baseUrl}${authReducer?.user?.picture}`,}}
                   />
                   <Text style={{fontSize:20, fontWeight:'bold'}}>{authReducer?.user?.lastname + ' '+ authReducer?.user?.firstname}</Text>
              </View>
              :
              <View style={styles.avatar}>
               <Avatar
                   rounded
                   activeOpacity={0.7}
                   showAccessory
                   onAccessoryPress={() => handleOpenImage()}
                   size={150}
                   source={image ? {uri: image.uri} : defaultUser}
                   />
                   <Text style={{fontSize:20, fontWeight:'bold'}}>{authReducer?.user?.lastname + ' '+ authReducer?.user?.firstname}</Text>
              </View>
           }
           <View style={styles.content}>
                <View style={styles.coverPaddingContent}>
                    <Text>Chỉnh sửa phần giới thiệu: </Text>
                    <View style={{paddingHorizontal:10}}>
                        <TextInput
                            multiline
                            placeholder='Introduce'
                            onChangeText={text => setIntroduce(text)}
                            value={introduce}
                        />
                        {/* <Text>dsa</Text> */}
                    </View>
                   
                </View>
                
           </View>
           <View style={styles.content}>
                <View style={styles.coverPaddingContent}>
                    <Text>Địa chỉ: </Text>
                    <View style={{paddingHorizontal:10}}>
                        <Text>Quê quán</Text>
                        <TextInput
                                multiline
                                placeholder='Quê quán của bạn'
                                onChangeText={text => setAddress(text)}
                                value={address}
                            />
                        <Text>Nơi ở hiện tại</Text>
                        <TextInput
                                multiline
                                placeholder='Nơi ở hiện tại của bạn'
                                onChangeText={text => setCurentAddress(text)}
                                value={currentAddress}
                            />
                        <Text>Lớp</Text>
                        <TextInput
                                placeholder='Tên lớp học hiện tại của bạn'
                                onChangeText={text => setNameClass(text)}
                                value={nameClass}
                            />
                    </View>   
                </View>
                
           </View>
           <View style={styles.content}>
                <View style={styles.coverPaddingContent}>
                    <Text>Số điện thoại:</Text>
                    <View style={{paddingHorizontal:10}}>
                        <Text>Di động</Text>
                        <TextInput
                                placeholder='Số di động hiện tại của bạn'
                                onChangeText={text => setSphone(text)}
                                value={sphone}
                            />
                        <Text>Máy bàn</Text>
                        <TextInput
                                placeholder='Số điện thoại nhà của bạn'
                                onChangeText={text => setCphone(text)}
                                value={cphone}
                            />
                    </View>
                </View>
           </View>
           <View style={styles.content}>
                <View style={styles.coverPaddingContent}>
                    <Text>Mail:</Text>
                    <View style={{paddingHorizontal:10}}>
                        <Text>Mail cá nhân</Text>
                            <TextInput
                                    placeholder='Mail cá nhân hiện tại của bạn'
                                    onChangeText={text => setMail(text)}
                                    value={mail}
                                />
                            <Text>Mail edu</Text>
                            <TextInput
                                    placeholder='Mail trường hiện tại của bạn'
                                    onChangeText={text => setEduGmail(text)}
                                    value={edumail}
                                />
                        </View>
                </View>
                
           </View>
        </ScrollView>
        <TouchableOpacity style={styles.button}>
             <Button onPress={handleUpdate} >Save</Button>
        </TouchableOpacity>
        <LoadHub hide={isLoading} />
    </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    avatar: {
        flexDirection:'column',
        alignItems:'center',
        justifyContent:'center',
        paddingVertical:20
    },
    content: {
        paddingVertical:10,
        paddingHorizontal:20,
        
    },
    coverPaddingContent: {
        //height:4000,
        borderWidth:1,
        paddingVertical:10,
        paddingHorizontal:10,
        borderRadius:10
    },
    button: {
        paddingVertical:10, 
        paddingHorizontal:20
    },
    coverButton: {
        borderWidth:1,
        padding: 8,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:15,
        //elevation: 0.1,
        backgroundColor:'#2089dc',
        borderColor:'#ccc'
    }
})
export default Introduce;