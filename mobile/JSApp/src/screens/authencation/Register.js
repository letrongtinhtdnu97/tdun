import React,{useEffect,useState} from 'react';
import {StyleSheet, Text, View, TextInput} from 'react-native'
import InputValue from '../../components/InputValue'
import Button from '../../components/Button'
import Icon from 'react-native-vector-icons/Feather'
import { CheckBox } from 'react-native-elements'
import {callRegister,callReset} from '../../redux/actions'
import {useDispatch, useSelector} from 'react-redux'
import registerReducer from '../../redux/reducers/register';

const Register = (props) => {
    const [checkPolicy, setPolicy] = useState(false)
    const [firstName, setFName] = useState('')
    const [lastName, setLName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [errorName, setErrorName] = useState('');
    const dispatch = useDispatch();
    const registerSelector = useSelector(state => state.registerReducer)
    
    useEffect(() => {
      if(registerSelector.isLoading) {
        setPolicy(false)
        setErrorName('')
        setFName('')
        setLName('')
        setEmail('')
        setPassword()
      }
      checkAcount()
    },[registerSelector])
    const checkAcount = () => {
        
        if(registerSelector.check === false && registerSelector.isSuccess === false) {
            timeOutError(`No create a account`)
        }
    }
    const timeOutError = (text) => {
        setErrorName(text)
        const timer = setTimeout(() => {
            setErrorName('')
        }, 3000);
        return () => clearTimeout(timer);
    }
    const handleCreateNewAccount = () => {
        if(!lastName || !firstName || !email || !password) {
              timeOutError('Fields input null')
              return false
        }
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(email) === false) {
            timeOutError('Email is Not Correct')
            return false;
        }
        // const data = {
        //     lastName = name,

        // }
        const postData = {
            lastname: lastName,
            firstname: firstName,
            email,
            password
        }
        return dispatch(callRegister(postData))
    }
    return (
        <View style={styles.container}>
            <View style={styles.textcontainer}>
                {
                    
                    !!errorName && <Text style={[styles.title, {color: 'red'}]}>*{errorName ? errorName : ''}</Text>
                }
                <Text style={styles.title}>First Name</Text>
                <View style={styles.inputContainer}>
                    <Icon name={'user'} size={25} color="#D3D4D5" />
                    <TextInput 
                        style={styles.input} 
                        value={firstName} 
                        onChangeText={(text) => setFName(text)}
                    />
                </View>
            </View>
            <View style={styles.textcontainer}>
                
                <Text style={styles.title}>Last Name</Text>
                <View style={styles.inputContainer}>
                    <Icon name={'user'} size={25} color="#D3D4D5" />
                    <TextInput 
                        style={styles.input} 
                        value={lastName} 
                        onChangeText={(text) => setLName(text)}
                    />
                </View>
            </View>
            <View style={styles.textcontainer}>
                <Text style={styles.title}>Email</Text>
                <View style={styles.inputContainer}>
                    <Icon name={'mail'} size={25} color="#D3D4D5" />
                    <TextInput 
                        style={styles.input} 
                        value={email} 
                        onChangeText={(text) => setEmail(text)}
                    />
                </View>
            </View>
            <View style={styles.textcontainer}>
                <Text style={styles.title}>Password</Text>
                <View style={styles.inputContainer}>
                    <Icon name={'lock'} size={25} color="#D3D4D5" />
                    <TextInput 
                        style={styles.input} 
                        value={password} 
                        onChangeText={(text) => setPassword(text)}
                    />
                </View>
            </View>
            
            <View style={styles.policy}>
               
                <CheckBox
                    onPress={() => setPolicy(!checkPolicy)}
                    containerStyle={{width:30}}
                    checked={checkPolicy}
                />
                <Text style={styles.policyText}>
                    I have read and agree the {' '}
                    <Text style={styles.policyTextColor}>Privacy Policy</Text>
                </Text>
            </View>
            <Button disabled={!checkPolicy} onPress={() => handleCreateNewAccount()} >CREATE ACCOUNT</Button>
        </View>
    );
};

const styles = StyleSheet.create({
    box: {
       width:20,
       height:20,
       backgroundColor:'#F8F9FA',
       borderColor:"#D3D4D5",
       borderRadius:4,
       borderWidth:1,
    },
    input: {
        flex: 1,
        marginLeft:5
    },
    inputContainer: {
        backgroundColor:'#F8F9FA',
        flexDirection: 'row',
        borderColor:"#D3D4D5",
        padding:6,
        borderRadius:4,
        borderWidth:1,
        alignItems:'center'
    },
    policy: {
        flexDirection:'row',
        alignItems:'center',
        paddingTop:10,
        marginBottom:40
    },
    policyText: {
        fontSize:16,
        color:'#9a9b9b',
        fontWeight:'500',
    },
    policyTextColor: {
        fontSize:15,
        color: "#e1aba8",
       
    }

})
export default Register;