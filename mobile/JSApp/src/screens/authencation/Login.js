import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, TextInput, TouchableOpacity} from 'react-native'
import InputValue from '../../components/InputValue'
import Button from '../../components/Button'
import Icon from 'react-native-vector-icons/Feather'
import {useSelector, useDispatch} from 'react-redux'
import {watchLoginUser} from '../../redux/actions'
import {getAvatar} from '../../redux/actions'
import Screens from '../../navigations/Screens'
import { useNavigation } from '@react-navigation/native';

const Login = () => {
    const [mail, setMail] = useState('')
    const [password, setPassword] = useState('')
    const [hidePass, setHidePass] = useState(true)
    const [textError, setTextError] = useState('')
    const [hide,setHide] = useState(false)
    const dispatch = useDispatch()
    const navigation = useNavigation()
    const authReducer = useSelector(state => state.authencationReducer)
    const loginUser = () => {
        const data = {
            email: mail,
            password
        }
        dispatch(watchLoginUser(data))
    }
    // useEffect(() => {
    //     const data = {
    //         userId: authReducer.user.id
    //     }
    //     dispatch(getAvatar(data))
    // },[authReducer])
    useEffect(() => {
        if(authReducer.connected){
            setMail('')
            setPassword('')
            
            return navigation.navigate(Screens.TAB)
        }
        if(authReducer.connecting) {
            setTextError('Mật khẩu hoặc tài khoản sai')
        }
    },[authReducer])
    return (
        <View>
            <View style={styles.container}>
                <Text style={styles.title}>Name</Text>
                <View style={styles.inputContainer}>
                    <Icon name={'mail'} size={25} color="#D3D4D5" />
                    <TextInput 
                        style={styles.input} 
                        value={mail} 
                        onChangeText={(text) => setMail(text)}
                    />
                </View>
            </View>
            <View style={styles.container}>
                <Text style={styles.title}>Password</Text>
                <View style={styles.inputContainer}>
                    <Icon name={'lock'} size={25} color="#D3D4D5" />
                    <TextInput 
                        style={styles.input} 
                        value={password}
                        secureTextEntry={hidePass}
                        onChangeText={(text) => setPassword(text)}
                    />
                    <TouchableOpacity onPress={()=>setHidePass(!hidePass)}>
                        <Icon name={hidePass ? 'eye-off' : 'eye'} size={25} color="#EC80B5" /> 
                    </TouchableOpacity>
                </View>
               
            </View>
            <Text style={styles.textError}>{mail && password&& textError}</Text>
            {/* <InputValue title="Password" icon="lock" isPassword={true} /> */}
            <Button style={styles.button} onPress={()=> loginUser()}>LOGIN</Button>
        </View>
    );
};
const styles = StyleSheet.create({
    
    button: {
        marginTop:40
    },
    input: {
        flex: 1,
        marginLeft:5
    },
    textError: {
        color:'red',
        paddingTop:5
    },
    title: {
        color: '#8A8B8C',
        fontWeight:"800",
        marginVertical: 8,
        fontSize:14
    },
    inputContainer: {
        backgroundColor:'#F8F9FA',
        flexDirection: 'row',
        borderColor:"#D3D4D5",
        padding:6,
        borderRadius:4,
        borderWidth:1,
        alignItems:'center'
    },

})
export default Login;