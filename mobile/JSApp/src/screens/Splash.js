import React, {useEffect} from 'react';
import {StyleSheet, Text, View, ScrollView, ActivityIndicator} from 'react-native'
import TextGradient from '../components/TextGradient'
import Button from '../components/Button'
import LottieView from 'lottie-react-native';
import { useNavigation } from '@react-navigation/native';
import Screens from '../navigations/Screens';
import {useSelector, useDispatch} from 'react-redux'
import {watchLoginUser} from '../redux/actions'
const Splash = (props) => {
    const navigation = useNavigation();
    const [user] = React.useState('')
    //const dispatch = useDispatch()
    const authReducer = useSelector(state => state.authencationReducer)
    useEffect(() => {
        const timer = setTimeout(() => {
            if(authReducer.connected){
                return navigation.navigate(Screens.TAB)
            }
            return navigation.navigate(Screens.AUTHENCATION)
          }, 3000);
          return () => clearTimeout(timer);
    },[])
    
    return (
        <View style={styles.container} >
            <TextGradient style={styles.title}>
                Welcome to App!!
            </TextGradient>
            <ActivityIndicator size="large" color="#eb64cf" style={styles.sub} />
            <View style={{flex:0.8}}>
                <LottieView
                        source={require('../assets/pin.json')}
                        colorFilters={[{
                            keypath: "button",
                            color: "#F00000"
                        },{
                        keypath: "Sending Loader",
                        color: "#F00000"
                    }]}
                        autoPlay
                        loop
                    />
                    
            </View>
            
            {/* <TextGradient style={styles.title}>
                Hello world
            </TextGradient>
            <Button style={styles.buttonNext}>Button</Button> */}
            <View style={styles.wapper}>
                <TextGradient>
                    Design by TinhLe.
                </TextGradient>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent:'center',
        
    },
    title: {
        alignSelf:'center', 
        fontSize:30,
        marginTop:40
    },
    sub: {
        
        paddingTop:15
    },
    buttonNext: {
        width:100, 
        height:40,
    },
    wapper: {
        alignSelf:'center',
        paddingTop:40,
    }
})
export default Splash;