import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, FlatList, TouchableOpacity,ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import ChatRoom from '../components/Chat'
import { ListItem,Avatar } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import mocks from '../mocks/chat.json'
import Screens from '../navigations/Screens'
import Nav from '../components/Profile/Nav'
const TimeLine = (props) => {
    const {navigation} = props
    const handleClick = (id) => {
            switch (id) {
                case '1':
                    return navigation.navigate(Screens.CHAT_ROOM,{id: 1}) 
                case '2':
                    return navigation.navigate(Screens.CHAT_ROOM,{id: 2}) 
                case '3':
                    return navigation.navigate(Screens.CHAT_ROOM,{id: 3}) 
                case '4':
                    return navigation.navigate(Screens.CHAT_ROOM,{id: 4}) 
                default:
                    break;
            }
    }
    const renderItem = (item) => {
        return(
            <ListItem
            Component={TouchableOpacity}
            onPress={() => handleClick(item.id)}
            friction={90} //
            tension={100} // These props are passed to the parent component (here TouchableScale)
            activeScale={0.95} //
            linearGradientProps={{
                colors: ['#FF9800', '#F44336'],
                start: { x: 1, y: 0 },
                end: { x: 0.2, y: 0 },
            }}
            ViewComponent={LinearGradient} // Only if no expo
            leftIcon={<Icon size={30} name={item.icon} />}
            title={item.name}
            titleStyle={{ color: 'white', fontWeight: 'bold' }}
            subtitleStyle={{ color: 'white' }}
            subtitle={item.sub}
            chevron={{ color: 'white' }}
            />
        );
    }
    return (
        // <ChatRoom name={name}  />
        <View>
            <Nav navigation={navigation} name={'Chat'} />
            <FlatList
            style={{paddingHorizontal:10, marginTop:20}}
            data={mocks}
            
            ItemSeparatorComponent={() => (
                <View style={{height:4, backgroundColor:'white'}} />
            )}
            renderItem={({ item }) => renderItem(item)}
            keyExtractor={item => item.id}
            
            />
            <View>
            <ScrollView style={{paddingHorizontal:10, paddingVertical:20}}>
                <Text style={{color:'red'}}>
                    *Để đảm bảo an toàn về thông tin các bạn lưu ý khi tham gia phòng chat:
                </Text>
                <Text>
                    1. Khi thoát app hoặc thoát khỏi room thì tất cả dữ liệu bạn đã chát sẽ xóa bỏ hoàn toàn
                </Text>
                <Text>
                    2. Nghiêm cấm các từ ngữ không đúng thuần phong mỹ tục. Các ngôn từ ảnh hưởng hoặc xúc phạm người khác.
                </Text>
                <Text>
                    3. Mọi thông tin lỗi app hoặc thành viên khác lăng mạ vui lòng chụp màn hình gởi về mail support@tdnu.com để được hỗ trợ giải quyết nhanh nhất
                </Text>
                <Text>
                    4. Mọi vấn đề liên quan khiếu nại, nhà trường là người quyết định cuối cùng.      
                </Text>    
            </ScrollView>
            </View>
        </View>
        
    );
};

export default TimeLine;