import React,{useState, useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import Avatar from '../../components/Avatar'
import Icon from 'react-native-vector-icons/Entypo'
const HomeDetail = (props) => {
    const {route, navigation} = props
    useEffect(()=>{
        console.log(route.params.id)
    },[])
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon style={{paddingRight:20}} name='chevron-left' size={30} />
                    </TouchableOpacity>
                    <Avatar size={50} id={route.params.id} />
                    <View style={{paddingLeft: 10}}>
                        <Text>Tinh le</Text>
                        <Text>9 h</Text>
                        
                    </View>
                </View>
                <TouchableOpacity>
                    <Icon name='dots-three-vertical' size={25} />
                </TouchableOpacity>
                
            </View>
            
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        height:70,
        width: 100+'%',
        paddingVertical:20,
        paddingHorizontal: 10,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between'
    }
})
export default HomeDetail;