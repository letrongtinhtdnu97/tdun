import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Screens from './Screens'
import Icon from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons'
//
import Authencation from '../screens/Authencation'
import Splash from '../screens/Splash'
import Home from '../screens/Home'

import Chat from '../screens/Chat'
import TimeLine from '../screens/TimeLine'
import Status from '../screens/CreateNew/Status'
import ChatRoom from '../components/Chat'

//profile
import Profile from '../screens/Profile'
import Fee from '../screens/ProfileDefault/Fee'
import Liabilities from '../screens/ProfileDefault/Liabilities'
import Term from '../screens/ProfileDefault/Term'
import Point from '../screens/ProfileDefault/Point'
import Introduce from '../screens/ProfileDefault/Introduce'

//home
import HomeDetail from '../screens/HomeDetail'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const StackHome = createStackNavigator();
const StackChat = createStackNavigator();
const StackProfile = createStackNavigator();
const optionDefault = () => {
    return {
        headerShown: false
    }
}
const TabProvider = () => {
    return(
        <Tab.Navigator screenOptions={({route}) => ({
            tabBarIcon: ({focused, color, size}) => {
                let colorIcon = focused ? 'red' : 'gray';
                let nameIcon;
                switch (route.name) {
                    case Screens.HOME:
                        nameIcon = 'home'
                        break;
                    case Screens.PROFILE:
                        nameIcon = 'contacts'
                        break;
                    case Screens.CHAT:
                        nameIcon = 'message1'
                        break;
                    case Screens.TIMELINE:
                        nameIcon = 'dashboard'
                        break;    
                    default:
                        break;
                }
                return <Icon name={nameIcon} size={size} color={colorIcon} />
            }
        })} tabBarOptions={{
            activeTintColor: 'tomato',
            inactiveTintColor: 'gray',
          }}>
                <Tab.Screen name={Screens.HOME} component={HomeProvider} options={{title: 'Home'}} />
                <Tab.Screen name={Screens.CHAT} component={ChatProvider} options={{title: 'Chat'}}/>
                <Tab.Screen name={Screens.TIMELINE} component={TimeLine} options={{title: 'Time line'}}/>
                <Tab.Screen name={Screens.PROFILE} component={ProfileProvider} options={{title: 'Profile'}}/>
            </Tab.Navigator>
    );
}
const ChatProvider = () => {
    return(
        <StackChat.Navigator>
            <StackChat.Screen name={Screens.CHAT} component={Chat} options={optionDefault} />
            <StackChat.Screen name={Screens.CHAT_ROOM} component={ChatRoom} options={optionDefault} />
        </StackChat.Navigator>
    );
}
const HomeProvider = () => {
    return(
        <StackHome.Navigator>
            <StackHome.Screen name={Screens.HOMEPAGE} component={Home} options={optionDefault} />
            <StackHome.Screen name={Screens.CREATE} component={Status}  options={optionDefault}/>
            <StackHome.Screen name={Screens.HOME_DETAIL} component={HomeDetail}  options={optionDefault}  />
        </StackHome.Navigator>
    );
}
const ProfileProvider = () => {
    return(
        <StackProfile.Navigator>
            <StackProfile.Screen name={Screens.PROFILE} component={Profile} options={optionDefault} />
            <StackProfile.Screen name={Screens.PROFILE_FEE} component={Fee} options={{title: 'Học phí của bạn'}} />
            <StackProfile.Screen name={Screens.PROFILE_TERM} component={Term} options={{title: 'Số tín chỉ của bạn'}} />
            <StackProfile.Screen name={Screens.PROFILE_LIABILITIES} component={Liabilities} options={{title: 'Công nợ của bạn'}} />
            <StackProfile.Screen name={Screens.PROFILE_POINT} component={Point} options={{title: 'Điểm thi của bạn'}}  />
            <StackProfile.Screen name={Screens.PROFILE_INTRODUCE} component={Introduce} options={{title: 'Chỉnh sửa cá nhân'}}  />
        </StackProfile.Navigator>
    )
}
const NavigationProvider = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name={Screens.SPLASH} component={Splash} options={optionDefault}  />
                <Stack.Screen name={Screens.TAB} component={TabProvider} options={optionDefault}  />
                <Stack.Screen name={Screens.AUTHENCATION} component={Authencation} options={optionDefault} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default NavigationProvider;