
import React,{useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import { Tooltip,Divider } from 'react-native-elements';
import Screens from '../navigations/Screens'
import {useSelector, useDispatch} from 'react-redux'
import {logout} from '../redux/actions'
const DATA = [
    {
        id: '1',
        title: "Thong bao truong",
        discription: "Hello tan sinh vien"
    },
    {
        id: '2',
        title: "Thong bao truong 1",
        discription: "Hello tan sinh vien1"
    },
    {
        id: '3',
        title: "Thong bao truong 2",
        discription: "Hello tan sinh vien2"
    },
    {
        id: '4',
        title: "Thong bao truong 3",
        discription: "Hello tan sinh vien3"
    }

]
const Notification = (props) => {
    const {iconMain,isLogin, iconSub,navigation} = props
    const dispatch = useDispatch()
    const authReducer = useSelector(state => state.authencationReducer)
    const handleClick = (id) => {
        switch (id) {
            case value:
                
                break;
        
            default:
                break;
        }
    }
    useEffect(() => {
        if(authReducer.connected === false) {
            return navigation.navigate(Screens.AUTHENCATION)
        }
    },[authReducer])
    const _logout = () => {
        dispatch(logout())
    }
    const _renderItem = (item) => {
        return(
            <View key={item.id}>
                <TouchableOpacity>
                    <View style={{height:50,  justifyContent:'center'}}>
                        <Text style={styles.textSetting}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
    return (
        <Tooltip 
                skipAndroidStatusBar={true}
                containerStyle={{
                    height: 200,
                    backgroundColor:'white',
                    width:200,
                    
                }}
                popover={ 
                    <View>
                        <FlatList 
                            data={DATA}
                            renderItem={({item}) => _renderItem(item)}
                            keyExtractor={item => item.id}
                        />
                       
                    </View>
                    

                }
                >
        <Icon name="notification" size={30} />
        </Tooltip>
    );
};
const styles = StyleSheet.create({
    setting: {
        flexDirection:'row', 
        alignItems:'center',
        
    },
    textSetting: {
        paddingLeft:8
    },
    tempNav: {
        width: 100+ '%',
        height:56,
        backgroundColor: "rgb(250,250,250)",
        borderBottomColor: "rgb(233,233,233)",
        borderBottomWidth: StyleSheet.hairlineWidth,
        justifyContent: 'space-between',
        flexDirection:'row',
        alignItems:'center'
        //alignItems: 'center'
        
        //backgroundColor:'red',
    },
    text: {
        fontWeight:'bold', 
        fontSize:20,
        paddingLeft:20
    },
})
export default Notification;