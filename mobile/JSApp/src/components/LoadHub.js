import React ,{ useState }from 'react';
import {View,Text, ActivityIndicator, StyleSheet} from 'react-native'
import { Button, Overlay } from 'react-native-elements';
const LoadHub = (props) => {
    const {hide} = props
    
    return (
        <View style={{position:'absolute'}}>
            <Overlay isVisible={hide}>
            <View style={styles.activityIndicatorWrapper}>
                <ActivityIndicator
                    animating={hide} 
                    size={'large'}
                />
            </View>
            </Overlay>
        </View>
    );
};
const styles = StyleSheet.create({
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
      }
})
export default LoadHub;