import React from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native'
import MaskedView from '@react-native-community/masked-view'
import LinearGradient from 'react-native-linear-gradient'
const TextGradient = props => {
    return (
        <MaskedView
        maskElement={
          <Text {...props}></Text>
        }
      >
        <LinearGradient
            colors={['#eb64cf','#ed736f']}
            start={{x:0,y:0}} 
            end={{x:1,y:0}}
        >
            <Text {...props} style={[props.style,{opacity:0}]}></Text>
        </LinearGradient>
      </MaskedView>
    );
};
const styles = StyleSheet.create({

})
export default TextGradient;