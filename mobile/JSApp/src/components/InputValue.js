import React, {useState} from 'react';
import {StyleSheet, View, Text, TextInput} from 'react-native'
import Icon from 'react-native-vector-icons/Feather'
const InputValue = ({title,icon,isPassword}) => {
    
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{title}</Text>
            <View style={styles.inputContainer}>
                <Icon name={icon} size={25} color="#D3D4D5" />
                <TextInput style={styles.input} />
                {isPassword && <Icon name="eye-off" size={25} color="#EC80B5" /> }
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        marginBottom:15
    },
    inputContainer: {
        backgroundColor:'#F8F9FA',
        flexDirection: 'row',
        borderColor:"#D3D4D5",
        padding:6,
        borderRadius:4,
        borderWidth:1,
        alignItems:'center'
    },
    title: {
        color: '#8A8B8C',
        fontWeight:"800",
        marginVertical: 8,
        fontSize:14
    },
    input: {
        flex: 1,
        marginLeft:5
    }
})
export default InputValue;