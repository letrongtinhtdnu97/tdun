import React, {useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity, Image} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import Avatar from '../../components/Avatar'
import Screens from '../../navigations/Screens'
const image = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Dogsledatrest.jpg/1200px-Dogsledatrest.jpg'
import Emoji from './Emoji'
const HeaderComment = (props) => {
    const {image,user, subUser, countUser, stt, tag,id, navigation,userId} = props
    const [like,setLike] = useState(false)
    const [countLike, setCountLike] = useState(0)
    return (
        <>
        <View style={styles.containerEmoji}>
            <View style={styles.box}>
                <TouchableOpacity onPress={() => {
                    if(like === true){
                        setLike(false)
                        setCountLike(countLike-1)
                    }
                    if(like === false){
                        setLike(true)
                        setCountLike(countLike+1)
                    }
                }}>
                    {
                        !like ? <Icon name={'heart-o'} size={30} /> : <Icon name={'heart'} size={30} color="#fc0362" />
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate(Screens.HOME_DETAIL,{id:id})} >
                    <Icon name={'comment-o'} size={30} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon name={'paper-plane-o'} size={30} />
                </TouchableOpacity>
            </View>
            <View style={styles.iconNote}>
                <TouchableOpacity>
                    <Icon name={'sticky-note-o'} size={30} />
                </TouchableOpacity>
            </View>
        </View>
        <View style={styles.container}>
            <View style={styles.stt}>
                <View>
                    <Avatar size={25} id={userId}   />
                </View>
                <View style={styles.contactName}>
                    <Text style={{fontWeight:'bold', marginHorizontal:5}}>
                        {user ? user : ''}
                    </Text>
                    <Text style={styles.marginContactText}>
                        {countLike}
                    </Text>
                    <Text style={styles.marginContactText}>
                        Like
                    </Text>
                    
                    
                </View>
            </View>
            <View style={styles.tag}>
                <View style={styles.contentTag}>
                    <Text>{user ? user: 'Admin'}:</Text>
                    <Text numberOfLines={3} style={styles.textStt}>{stt ? stt : ''}</Text>
                </View>
                
                <TouchableOpacity onPress={() => navigation.navigate(Screens.HOME_DETAIL,{id: id})}>
                    <Text style={{color:'gray'}}>View all comments</Text>
                </TouchableOpacity>
            </View>
        </View>
        
        </>
        
    );
};
const styles = StyleSheet.create({
    containerEmoji: {
        flexDirection: 'row',
        justifyContent:'space-between',
        
    },
    box: {
        flexDirection: 'row',
        paddingVertical:8,
        paddingHorizontal:8,
        width:150,
        justifyContent:'space-between',
        alignItems:'center'
    },
    iconNote: {
        paddingVertical:8,
        paddingHorizontal:8,
    },
    container: {
        paddingHorizontal:18,
        paddingVertical: 8
    },
    marginContactText: {
        marginHorizontal:2
    },
    tag: {
        paddingVertical: 8,
        paddingHorizontal:8
    },
    textStt: {
        paddingLeft:4, 
        paddingRight:20, 
        fontSize:14, 
        color:'gray'
    },
    contentTag: {
        flexDirection:'row',
        marginRight:20
    },
    contactName: {
        flexDirection:'row'
    },
    stt: {
        flexDirection:'row',
        alignItems:'center'
    }
})
export default HeaderComment;