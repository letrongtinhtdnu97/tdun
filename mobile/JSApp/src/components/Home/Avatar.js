import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity,Image} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
const image = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Dogsledatrest.jpg/1200px-Dogsledatrest.jpg'
const baseUrl = "http://192.168.1.19:3003/images/"
const Avatar = (props) => {
    const {uri, size,borderRadius, navigation} = props
    //alert(uri)
    return (
        <TouchableOpacity onPress={() => console.log('avatar')}>
            <Image
            //size={size}
                style={{height:size, width:size, borderRadius:borderRadius}}
                source={{
                uri: uri ? uri : image
                }}
            />
        </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
    container: {},
    image: {
        height:20, 
        width:20, 
        backgroundColor:20, 
        //borderRadius:20
    }
})
export default Avatar;