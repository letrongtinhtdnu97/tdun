import React,{useState, useEffect} from 'react';
import {View,Image,Dimensions, StyleSheet,Text} from 'react-native'
import Emoji from './Emoji'
import HeaderComment from './HeaderComment'
import Icon from 'react-native-vector-icons/Entypo'
import moment from 'moment'
import Avatar from '../Avatar'
const image = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Dogsledatrest.jpg/1200px-Dogsledatrest.jpg'
const baseUrl = "http://192.168.1.19:3003/images/"
const screenHeight = Dimensions.get('window').height
const screenWith =Dimensions.get('window').width
const Status = (props) => {
    const {data, navigation} = props
    const {address, createAt, emoji, flag,imagesId, title, userId, _id} = data
    const {firstname, lastname} = userId
    const [state,setState] = useState(emoji)
    const [name, setName] = useState('')
    const imageShow = imagesId.length > 0 && `${baseUrl}${imagesId[0].name}` 
    useEffect(() => {
        switch (emoji) {
            case 1:
                setName('vui vẻ')
                    break;
            case 2:
                    setName('hạnh phúc')
                    break;
            case 3:
                    setName('buồn')
                    break;
            case 4:
                    setName('thoải mái')
                    break;
            default:
                setName('')
                break;
        }
    },[state])
    return (
        <>
            <View style={styles.subContainer}>
                <View style={styles.containerContent}>
                            <Avatar size={50} id={userId} />
                            <View>
                                <Text style={styles.textName}>
                                    {lastname || firstname ? (firstname ? firstname : null) + ' '+ (lastname ? lastname : null)  : '' } 
                                    {' '}
                                    {name ? <Text>is {name ? name : ''}</Text> : null}
                                    {' '}
                                    {address ? <Text>in {address? address : ''}</Text> : null}
                                </Text>
                                <Text style={{paddingLeft:10}}>{moment(createAt ? createAt : new Date()).startOf('hour').fromNow()}</Text>
                            </View>
                </View>
                <View style={styles.menu}>
                        <Icon style={styles.iconAvatar} name="dots-three-horizontal" size={25} />
                </View>

            </View>
            { imageShow &&<View style={styles.wrapper}>
                <Image
                    style={styles.image}
                    resizeMode='cover'
                    source={{
                        uri: imageShow
                    }}
                />
                </View>
            }
            <View>
                
                <HeaderComment 
                    userId={userId}
                    navigation={navigation}
                    id={_id}
                    user={lastname || firstname ? (firstname ? firstname : null) + ' '+ (lastname ? lastname : null)  : null } 
                    stt={title? title : ''}
                />
            </View>
        </>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1, 
        width: 100 + "%", 
        height: 100 + "%"
    },
    subContainer: {
        flexDirection:'row', 
        alignItems:'center', 
        justifyContent:'space-between'
    },
    iconAvatar: {
        //alignSelf:'flex-start'
        paddingRight:25
    },
    menu: {
        alignItems:'flex-end',
    },
    containerContent: {
        paddingVertical:8,
        paddingHorizontal: 8,
        flexDirection: 'row',
        alignItems:'center',
        
    },
    textName: {
        paddingLeft:8,
        fontSize:16,
        fontWeight:'bold'
    } ,
    image: {
        width: 100 + "%", 
        height: screenHeight/2.3
    },
    wrapper: {
        paddingTop: 5
    },
    avatar: {
        height:40,
        width:40,
        borderRadius:40
    }
})
export default Status;