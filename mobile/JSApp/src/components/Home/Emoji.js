import React, {useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native'
import Screens from '../../navigations/Screens'
import Icon from 'react-native-vector-icons/FontAwesome'
//heart-o]
import HeaderComment from './HeaderComment'
const Emoji = (props) => {
    
    const {iconHeart, iconLike, iconShare, navigation,id} = props
    const [like,setLike] =useState(false)
    return (
        <View style={styles.container}>
            <View style={styles.box}>
                <TouchableOpacity onPress={() => setLike(!like)}>
                    {
                        !like ? <Icon name={'heart-o'} size={30} /> : <Icon name={'heart'} size={30} color="#fc0362" />
                    }
                </TouchableOpacity>
                <TouchableOpacity onPress={() => navigation.navigate(Screens.HOME_DETAIL,{id:id})} >
                    <Icon name={'comment-o'} size={30} />
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon name={'paper-plane-o'} size={30} />
                </TouchableOpacity>
            </View>
            <View style={styles.iconNote}>
                <TouchableOpacity>
                    <Icon name={'sticky-note-o'} size={30} />
                </TouchableOpacity>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent:'space-between',
        
    },
    box: {
        flexDirection: 'row',
        paddingVertical:8,
        paddingHorizontal:8,
        width:150,
        justifyContent:'space-between',
        alignItems:'center'
    },
    iconNote: {
        paddingVertical:8,
        paddingHorizontal:8,
    }
})
export default Emoji;