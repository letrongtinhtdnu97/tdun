import React,{useEffect, useState} from 'react';
import {View, Text, Image} from 'react-native'
import axios from 'axios'
import {defaultUser,baseUrl} from '../assets'
const Avatar = (props) => {
    const {id,size} = props
    const [image, setImage] = useState(null)
    
    useEffect(() => {
        const data = {
            userId: id
        }
        getApi(data)
    },[])
    const getApi = async(data) => {
        axios.post('http://192.168.1.19:3003/v1/auth/get-avatar',data)
        .then((rs) =>{
            rs?.data?.data?.picture ? setImage(rs?.data?.data?.picture) : setImage(null)
        })
        .catch((e) => setImage(null))
    }
   
    return (
        <View>
            <Image style={{width: size, height: size, borderRadius:size,}} source={ image ? {uri: `${baseUrl}${image}`} : defaultUser} />
        </View>
    );
};

export default Avatar;