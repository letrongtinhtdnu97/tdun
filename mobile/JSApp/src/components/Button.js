import React from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
const Button = ({children, style, onPress, disabled}) => {
    return (
        <TouchableOpacity disabled={disabled} onPress={onPress}>
            <LinearGradient 
            colors={['#eb64cf','#ed736f']}
            start={{x:0,y:0}} 
            end={{x:1,y:0}}
            style={[styles.gradient,style]}
            >
                <Text style={styles.text}>{children}</Text>
            </LinearGradient>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    text: {
        fontSize:17,
        fontWeight:'600',
        color:'#fff'
    },
    gradient: {
        padding:10,
        paddingVertical:13,
        borderRadius:5,

        justifyContent:'center',
        alignItems:'center'
    }
})
export default Button;