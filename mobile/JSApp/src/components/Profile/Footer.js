import React from 'react';
import {StyleSheet, Text, View, ScrollView, Modal, TouchableOpacity,FlatList} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Screens from '../../navigations/Screens'
import mocks from '../../mocks/footer_profile.json'
const Footer = (props) => {
    const {navigation} = props

    const swapNavigation = (id) => {
        switch (id) {
            case '1':
                return navigation.navigate(Screens.PROFILE_POINT)
            case '2':
                return navigation.navigate(Screens.PROFILE_TERM)
            case '3':
                return navigation.navigate(Screens.PROFILE_FEE)
            case '4':
                return navigation.navigate(Screens.PROFILE_LIABILITIES)
            default:
                return console.log('null')
                break;
        }
        
    }
    const ItemFooter = (item) => {
        if(!item) {
            return null
        }
        return(
            <TouchableOpacity key={item.id} style={styles.borderOvan} onPress={() => swapNavigation(item.id)} >
                <LinearGradient 
                    colors={[item.colorLeft,item.colorRight]}
                    start={{x:0,y:0}} 
                    end={{x:1,y:0}}
                    style={styles.ovan}
                >
                    <Text>{item.name}</Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    }
    return (
        <View style={{ paddingTop:20, paddingBottom:20}} >
                    <FlatList 
                        style={{flexDirection:'row', justifyContent:'space-around'}}
                        data={mocks}
                        renderItem={({ item }) => ItemFooter(item)}
                        keyExtractor={item => item.id}
                    />
                </View>
    );
};
const styles = StyleSheet.create({
    borderOvan: {
        width:82, height:82, borderRadius:40, backgroundColor:'white'
    },
    ovan: {
        width:80, height:80, borderRadius:40,  alignItems:'center', justifyContent:'center'
    }

})
export default Footer;