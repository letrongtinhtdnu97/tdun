import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
const Nav = (props) => {
    const {navigation, name} = props
    return (
        <View style={styles.nav} >
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Icon  name="left" size={30} />
            </TouchableOpacity>
            <Text style={styles.textHeader}>{name}</Text>
            <View />
        </View>
    );
};

const styles = StyleSheet.create({
    textHeader: {
        fontSize:20,
        fontWeight:'bold'
    },
    nav: {
        width:100+'%',
        height:55,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        paddingHorizontal:10
    },
})
export default Nav;