import React,{useState,useEffect} from 'react';
import { View,Text, Image,StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import {Avatar as Avatar1} from 'react-native-elements'
import ImagePicker from 'react-native-image-crop-picker';
import Screens from '../../navigations/Screens'
import {useSelector} from 'react-redux'
const images1 = "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Dogsledatrest.jpg/1200px-Dogsledatrest.jpg"
const baseUrl = "http://192.168.1.19:3003/images/"
import {defaultUser, tick} from '../../assets'
const Avatar = props => {
    const {name, navigation} = props
    const authSelector = useSelector(state => state.authencationReducer)
    const [image, setImages] = useState(null)
    const [saveImage, setSaveImage] = useState(false)

    useEffect(() => {
        authSelector?.user?.picture ? setImages(authSelector.user.picture)  :  setImages(null)
    },[authSelector])
    const OpenNavigation  = () => {
        return navigation.navigate(Screens.PROFILE_INTRODUCE)
    }
    // const handleOpenImage = async() => {
    //     ImagePicker.openPicker({
    //         width: 300,
    //         height: 400,
    //         cropping: true
    //       }).then(images => {
    //         setImages(images)
    //         setSaveImage(true)
    //     });
    // }
    return (
        <View style={styles.container}>
            
            <Image style={styles.coverBGTop} source={{uri: images1 }}/>
            <Icon style={{position:'absolute', right:20, top:165}} name="camera" size={30} />
            <View style={{alignItems:'center', justifyContent:'center', paddingTop:20}}>
                <View style={styles.avatar}>
                    {
                        image ?
                        <Avatar1
                        rounded
                        containerStyle={{backgroundColor:'#cecece'}}
                        size={150}
                        //showAccessory
                        onPress={() => OpenNavigation()}
                        //onAccessoryPress={() => OpenNavigation()}
                        source={{uri:image ? `${baseUrl}${image}` : defaultUser }}
                    /> :
                    <Avatar1
                        rounded
                        containerStyle={{backgroundColor:'#cecece'}}
                        size={150}
                        title={name.substr(0,1).toUpperCase()}
                        onPress={() => OpenNavigation()}
                        source={defaultUser}
                    />
                    }
                    
                    
                </View>
            </View>
            <Text style={styles.textName}>{name}</Text>

        </View>
    );
};
const styles = StyleSheet.create({
    
    container: {
        flex: 1,
    },
    avatar: {
        alignItems:'center',
        paddingVertical:10,
        paddingHorizontal: 10,
        position:'absolute',
        backgroundColor:'white',
        borderRadius:100,

       
    },
    textName: {
        fontSize:20, 
        fontWeight:'bold',
        paddingTop:90,
        alignSelf:'center'
    },
    coverBGTop: {
        width:100+'%',
        height: 200
    }
})
export default Avatar;