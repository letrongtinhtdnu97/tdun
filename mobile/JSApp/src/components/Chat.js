import React, { useEffect, useState } from 'react';
import {StyleSheet, Text, View, ScrollView, TouchableOpacity} from 'react-native'
import { WebView } from 'react-native-webview';
import {useSelector} from 'react-redux'
import Icon from 'react-native-vector-icons/AntDesign'
import mocks from '../mocks/chat.json'
const ChatRoom = (props) => {
    const {route, navigation} = props
    const [arr, setArr] = useState(0)
    const authReducer = useSelector(state => state.authencationReducer)
    const [fullName, setFullName] = useState('')
    const [room, setRoom] = useState('')
    const all = 'Tất cả mọi người'
    useEffect(() => {
        setArr(mocks[route.params.id-1])
        authReducer?.user ? setFullName(authReducer.user.firstname + ' ' + authReducer.user.lastname) : setFullName('')
    },[])
    if(!authReducer) {
        return(
            null
        );
    }
    return (
        <>
        <View style={{width:100+'%', height:59, alignItems:'center', justifyContent:'space-between', flexDirection:'row', paddingHorizontal:10, backgroundColor:'#34a1eb'}}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
                <Icon name="left" size={30} />
            </TouchableOpacity>
            <Text style={{fontSize:20, fontWeight:'bold'}}>{arr.sub ? arr.sub : all}</Text>
            <View></View>
        </View>
        <WebView source={{ uri: `https://client-chat-web.herokuapp.com/chat?name=${fullName ? fullName : 'Không tên'}&room=${arr.sub ? arr.sub : all}` }} />
        </>
    );
};

export default ChatRoom;