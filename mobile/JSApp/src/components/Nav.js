import React, {useState} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import Screens from '../navigations/Screens'
import ToolTip from './ToolTip'
import Notification from './Notification'
const Nav = (props) => {
    const {navigation, heart, camera} = props
    const [isHeart] = useState(heart) 
    const [isCamera] = useState(camera) 
    return (
        <View style={styles.tempNav}>
            <Text style={styles.text}>INSTAGRAM</Text>
            <View style={{flexDirection:'row', justifyContent: heart || camera ? 'space-between' : 'flex-end', width:130, paddingRight:20}}>
                {
                    !!isHeart &&<Notification iconSub={[{id: '1',icon: 'user',name: 'Profile'}, {id: '2', icon: 'sync', name: 'Saved'}]} navigation={navigation} />
                }
                {
                    !!isCamera  && (
                        <TouchableOpacity onPress={() => navigation.navigate(Screens.CREATE)}>
                            <Icon name="instagram" size={30} />
                        </TouchableOpacity> 
                    ) 
                } 
                <ToolTip 
                    iconSub={[{id: '1',icon: 'user',name: 'Profile'}, {id: '2', icon: 'sync', name: 'Saved'}]} 
                    isLogin={true} 
                    navigation={navigation}
                />
            </View>
            
        </View>
        
    );
};

const styles = StyleSheet.create({
    setting: {
        flexDirection:'row', 
        alignItems:'center',
        height:30
    },
    textSetting: {
        paddingLeft:8
    },
    tempNav: {
        width: 100+ '%',
        height:56,
        backgroundColor: "rgb(250,250,250)",
        borderBottomColor: "rgb(233,233,233)",
        borderBottomWidth: StyleSheet.hairlineWidth,
        justifyContent: 'space-between',
        flexDirection:'row',
        alignItems:'center'
        //alignItems: 'center'
        
        //backgroundColor:'red',
    },
    text: {
        fontWeight:'bold', 
        fontSize:20,
        paddingLeft:20
    },
})
export default Nav;