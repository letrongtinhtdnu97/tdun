import React,{useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import { Tooltip,Divider } from 'react-native-elements';
import Screens from '../navigations/Screens'
import {useSelector, useDispatch} from 'react-redux'
import {logout} from '../redux/actions'

const ToolTip = (props) => {
    const {iconMain,isLogin, iconSub,navigation} = props
    const dispatch = useDispatch()
    const authReducer = useSelector(state => state.authencationReducer)
    const handleClick = (id) => {
        switch (id) {
            case value:
                
                break;
        
            default:
                break;
        }
    }
    useEffect(() => {
        if(authReducer.connected === false) {
            return navigation.navigate(Screens.AUTHENCATION)
        }
    },[authReducer])
    const _logout = () => {
        dispatch(logout())
    }
    const swapNavigation = (id) => {
        if(id === '1') {
            return navigation.navigate(Screens.PROFILE)
        }
    }
    const _renderItem = (item) => {
        return(
            <View key={item.id}>
                <TouchableOpacity onPress={() =>swapNavigation(item.id) }>
                    <View style={styles.setting}>
                        <Icon name={item.icon} size={20} />
                        <Text style={styles.textSetting}>{item.name}</Text>
                    </View>
                </TouchableOpacity>
                <Divider style={{ backgroundColor: 'gray' }} />
            </View>
        );
    }
    return (
        <Tooltip 
                skipAndroidStatusBar={true}
                containerStyle={{
                    height: iconSub.lenght === 3 ? 150 : 120,
                    backgroundColor:'white'
                }}
                popover={ 
                    <View>
                        <FlatList 
                            data={iconSub}
                            renderItem={({item}) => _renderItem(item)}
                            keyExtractor={item => item.id}
                        />
                        {
                            !!isLogin && (
                                <>
                                <Divider style={{ backgroundColor: 'gray' }} />
                                <TouchableOpacity onPress={() => _logout()}>
                                    <View style={styles.setting}>
                                        <Text style={styles.textSetting}>Logout</Text>
                                    </View>
                                </TouchableOpacity>
                                </>
                            )
                        }
                    </View>
                    

                }
                >
        <Icon name="setting" size={30} />
        </Tooltip>
    );
};
const styles = StyleSheet.create({
    setting: {
        flexDirection:'row', 
        alignItems:'center',
        height:30
    },
    textSetting: {
        paddingLeft:8
    },
    tempNav: {
        width: 100+ '%',
        height:56,
        backgroundColor: "rgb(250,250,250)",
        borderBottomColor: "rgb(233,233,233)",
        borderBottomWidth: StyleSheet.hairlineWidth,
        justifyContent: 'space-between',
        flexDirection:'row',
        alignItems:'center'
        //alignItems: 'center'
        
        //backgroundColor:'red',
    },
    text: {
        fontWeight:'bold', 
        fontSize:20,
        paddingLeft:20
    },
})
export default ToolTip;