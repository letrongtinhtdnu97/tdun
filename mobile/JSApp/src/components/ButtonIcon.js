import React from 'react';
import {StyleSheet, Text, View} from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'

const ButtonIcon = ({icon, color}) => {
    return (
        <View style={styles.container}>
            <Icon name={icon} size={30} color={color} />
        </View>
    );
};


const styles = StyleSheet.create({
    container: {
        width:40,
        height:40,
        borderWidth:1,
        borderRadius:40,
        justifyContent:'center',
        alignItems:'center'
    }
})

export default ButtonIcon;