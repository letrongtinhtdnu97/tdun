import {
	put, call, takeLatest
} from 'redux-saga/effects';
import {createNewUser, getAvatarUser} from '../apis/register'
import {
    REGISTER_SUCCESS,REGISTER_FAIL,REGISTER_CREATE,
    REGISTER_RESET,
    AVATAR_FETCH,AVATAR_FAIL,AVATAR_SUCCESS} from '../actions/register'
function* callRegister(action) {
    try {
        const user = yield call(createNewUser,action)
        yield put({
            type: REGISTER_SUCCESS, user
        })
    } catch (error) {
        yield put({
            type: REGISTER_FAIL, error
        })
    }
}

function* callResetState() {
    const data = {
        type: 'reset'
    }
    yield put({
        type: REGISTER_SUCCESS, data
    })
}
function* callGetAvatar() {
    try {
        const avatar = yield call(getAvatarUser,action)
        yield put({
            type: REGISTER_SUCCESS, avatar
        })
    } catch (error) {
        yield put({
            type: REGISTER_FAIL, error
        })
    }
}
export function* watchCallGetAvatar(){
    yield takeLatest(AVATAR_FETCH, callGetAvatar)
}
export function* watchResetState() {
    yield takeLatest(REGISTER_RESET,callResetState)
}
export function* watchCreateNewUser() {
    yield takeLatest(REGISTER_CREATE,callRegister)
}