import {
	put, call, takeLatest
} from 'redux-saga/effects';
import {getAllSchedule} from '../apis/schedule'
import {SCHEDULE_SUCCESS,SCHEDULE_FETCH,SCHEDULE_FAIL} from '../actions/schedule'
function* callScheduleSaga(action) {
    try {
        const schedule = yield call(getAllSchedule,action)
        yield put({
            type: SCHEDULE_SUCCESS, schedule
        })
    } catch (error) {
        yield put({
            type: SCHEDULE_FAIL, error
        })
    }
}


export function* watchCallSchedule() {
    yield takeLatest(SCHEDULE_FETCH,callScheduleSaga)
}