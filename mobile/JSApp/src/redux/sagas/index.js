import { all } from 'redux-saga/effects';
import {watchCallUserLogin, watchCallLogout} from './authencation'
import {watchCreateNewUser,watchResetState,watchCallGetAvatar} from './register'
import {watchFetchStatusHome} from './home'
import {watchCallSchedule} from './schedule'
import {watchFetchUser,watchUpdateUser} from './profile'
const root = function* root() {
    yield all([
        watchCallUserLogin(),
        watchCallLogout(),

        watchCreateNewUser(),
        watchResetState(),
        watchCallGetAvatar(),


        watchFetchStatusHome(),

        watchCallSchedule(),

        //profile
        watchFetchUser(),
        watchUpdateUser(),
    ])
}

export default root;