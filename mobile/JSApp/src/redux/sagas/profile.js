import {
	put, call, takeLatest
} from 'redux-saga/effects';
import {getByUser, updateByUser} from '../apis/profile'
import {PROFILE_SUCCESS,PROFILE_UPDATE,PROFILE_FAIL,PROFILE_FETCH} from '../actions/profile'

function* fetchUser(action) {
    try {
        const user = yield call(getByUser,action)
        yield put({
            type: PROFILE_SUCCESS, user
        })
    } catch (error) {
        yield put({
            type: PROFILE_FAIL, error
        })
    }
}

function* updateUser(action) {
    try {
        console.log("action---sagas",action)
        const user = yield call(updateByUser,action)
        user.type = 'update'
        yield put({
            type: PROFILE_SUCCESS, user
        })
    } catch (error) {
        yield put({
            type: PROFILE_FAIL, error
        })
    }
}


export function* watchFetchUser() {
    yield takeLatest(PROFILE_FETCH,fetchUser)
}

export function* watchUpdateUser() {
    yield takeLatest(PROFILE_UPDATE,updateUser)
}