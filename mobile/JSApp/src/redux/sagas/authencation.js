import {
	put, call, takeLatest, select, take, fork, cancel
} from 'redux-saga/effects';
import {watchLoginUser} from '../apis/authencation'
import {AUTH_FETCH_DATA,AUTH_FETCH_SUCCESS,AUTH_FETCH_FAIL, AUTH_LOGOUT} from '../actions/authencation'
function* callUserLogin(action) {
    try {
        const user = yield call(watchLoginUser,action)
        yield put({
            type: AUTH_FETCH_SUCCESS, user
        })
    } catch (error) {
        yield put({
            type: AUTH_FETCH_FAIL, error
        })
    }
}

function* callLogout(action) {
    const data = {connecting: false, connected: false, type: 'logout'}
    yield put({
        type: AUTH_FETCH_SUCCESS, data
    })
}
export function* watchCallLogout() {
    yield takeLatest(AUTH_LOGOUT, callLogout)
}
export function* watchCallUserLogin() {
    yield takeLatest(AUTH_FETCH_DATA,callUserLogin)
}