import {
	put, call, takeLatest
} from 'redux-saga/effects';
import {fetchStatus} from '../apis/home'
import {HOME_FAIL,HOME_FETCH,HOME_SUCCESS} from '../actions/home'
function* fetchStatusHome(action) {
    try {
        console.log("action",action)
        const status1 = yield call(fetchStatus,action)
        yield put({
            type: HOME_SUCCESS, status1
        })
    } catch (error) {
        yield put({
            type: HOME_FAIL, error
        })
    }
}


export function* watchFetchStatusHome() {
    yield takeLatest(HOME_FETCH,fetchStatusHome)
}