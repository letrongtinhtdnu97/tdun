import {combineReducers } from 'redux'
import authencationReducer from './authencation'
import registerReducer from './register'
import homeReducer from './home'
import scheduleReducer from './schedule'
import profileReducer from './profile'
const allReducers =  combineReducers({
    authencationReducer,
    registerReducer,
    homeReducer,
    scheduleReducer,
    profileReducer
});
export default allReducers;