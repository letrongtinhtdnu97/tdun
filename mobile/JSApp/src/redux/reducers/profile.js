import {PROFILE_FETCH,PROFILE_FAIL,PROFILE_UPDATE,PROFILE_SUCCESS} from '../actions/profile'
const initialState = {
    isLoading: false,
    data: []
};

const profileReducer = (state = initialState, action) => {
    
    switch (action.type){
        case PROFILE_FETCH:
            return {
                ...state
            }
        case PROFILE_UPDATE:
            console.log('PROFILE_UPDATE', action)
            return {
                ...state,
                isLoading: false,
            }    
        case PROFILE_SUCCESS: 
            console.log('PROFILE_SUCCESS', action)
            return {
                ...state,
                isLoading: true,
                data: action.user.data
               
            }
        case PROFILE_FAIL: 
            return {
                ...state,
                data: [],
                isLoading: true,
            }
        default: 
            return {
                ...state
            }
    }
}

export default profileReducer