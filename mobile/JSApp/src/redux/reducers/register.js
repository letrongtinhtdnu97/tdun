import {REGISTER_CREATE,REGISTER_FAIL,REGISTER_SUCCESS} from '../actions/register'
const initialState = {
    isSuccess: false,
    isLoading: false,
    check: true
};

const registerReducer = (state = initialState, action) => {
    switch (action.type){
        case REGISTER_CREATE:
            return {
                ...state
            }
        case REGISTER_SUCCESS: 
            if(action.data.type === 'reset'){
                return {
                    ...state,
                    isLoading: false,
                    isSuccess: false,
                    check: false
                }
            } 
            return {
                ...state,
                isLoading: true,
                isSuccess: true,
                check: true
               
            }
        case REGISTER_FAIL: 
            return {
                ...state,
                isLoading: true,
                check: false,
            }
        default: 
            return {
                ...state
            }
    }
}

export default registerReducer