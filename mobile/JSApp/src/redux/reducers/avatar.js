import {AVATAR_SUCCESS,AVATAR_FAIL,AVATAR_FETCH} from '../actions/register'
const initialState = {
    
    isLoading: false,
    data: []
};

const avatarReducer = (state = initialState, action) => {
    switch (action.type){
        case AVATAR_FETCH:
            return {
                ...state
            }
        case AVATAR_SUCCESS: 
           console.log('AVATAR_SUCCESS',action.avatar)
            return {
                ...state,
                isLoading: true,
                    
            }
        case AVATAR_FAIL: 
            return {
                ...state,
                isLoading: true,
            }
        default: 
            return {
                ...state
            }
    }
}

export default avatarReducer