import {AUTH_FETCH_DATA,AUTH_FETCH_SUCCESS,AUTH_FETCH_FAIL} from '../actions/authencation'
const initialState = {
	connecting: false,
    connected: false,
    user: '',
    token: ''
};

const authencationReducer = (state = initialState, action) => {
    switch (action.type){
        case AUTH_FETCH_DATA:
            return {
                ...state
            }
        case AUTH_FETCH_SUCCESS: 
            if(action?.data?.type === 'logout') {
                return {
                    ...state,
                    connected: false,
                    connecting: false,
                    user: '',
                    token: ''
                }
            }
            return {
                ...state,
                connected: true,
                user: action.user.data.user,
                token: action.user.data.token
            }
        case AUTH_FETCH_FAIL: 
            return {
                ...state,
                connecting: true
            }
        default: 
            return {
                ...state
            }
    }
}

export default authencationReducer