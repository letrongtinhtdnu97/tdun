import {HOME_FETCH,HOME_FAIL,HOME_SUCCESS} from '../actions/home'
const initialState = {
    isLoading: false,
    data: []
};

const homeReducer = (state = initialState, action) => {
    switch (action.type){
        case HOME_FETCH:
            return {
                ...state
            }
        case HOME_SUCCESS: 
            return {
                ...state,
                isLoading: true,
                data: action.status1.data.data
               
            }
        case HOME_FAIL: 
            return {
                ...state,
                data: [],
            }
        default: 
            return {
                ...state
            }
    }
}

export default homeReducer