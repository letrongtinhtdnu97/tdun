import {SCHEDULE_FAIL,SCHEDULE_FETCH,SCHEDULE_SUCCESS} from '../actions/schedule'
const initialState = {
    isLoading: false,
    data: []
};

const scheduleReducer = (state = initialState, action) => {
    switch (action.type){
        case SCHEDULE_FETCH:
            return {
                ...state
            }
        case SCHEDULE_SUCCESS: 
            return {
                ...state,
                isLoading: true,
                data: action.schedule.data
               
            }
        case SCHEDULE_FAIL: 
            return {
                ...state,
                isLoading: true,
                check: false,
            }
        default: 
            return {
                ...state
            }
    }
}

export default scheduleReducer