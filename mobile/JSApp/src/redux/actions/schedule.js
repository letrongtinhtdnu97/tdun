export const SCHEDULE_FETCH = '@schedule/fetch'
export const SCHEDULE_SUCCESS = '@schedule/success'
export const SCHEDULE_FAIL = '@schedule/fail'