export const REGISTER_CREATE ='@register/call'
export const REGISTER_SUCCESS = '@register/success'
export const REGISTER_FAIL = '@register/fail'

export const REGISTER_RESET = '@register/reset'

export const AVATAR_FETCH = '@avatar/fetch'
export const AVATAR_SUCCESS = '@avatar/success'
export const AVATAR_FAIL = '@avatar/fail'