export const PROFILE_FETCH = "@profile/fetch"
export const PROFILE_SUCCESS = "@profile/success"
export const PROFILE_FAIL = "@profile/fail"
export const PROFILE_UPDATE = "@profile/update"
export const getProfileUser = (data) => {
    console.log('data------action',data)
    return {
        type: PROFILE_FETCH,
        data
    }
}

export const updateProfileUser = (data) => {
    return {
        type: PROFILE_UPDATE,
        data
    }
}