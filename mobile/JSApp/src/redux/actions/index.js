import {AUTH_FETCH_DATA, AUTH_LOGOUT} from './authencation'
import {REGISTER_CREATE, REGISTER_RESET, AVATAR_FETCH} from './register'
import {SCHEDULE_FETCH} from './schedule'

export const watchLoginUser = (data) => {
    
    return {
        type: AUTH_FETCH_DATA,
        data
    }
}

export const logout = () => {
    return {
        type: AUTH_LOGOUT
    }
}

export const callRegister = (data) => {
    return {
        type: REGISTER_CREATE,
        data
    }
}

export const callReset = () => {
    return {
        type: REGISTER_RESET
    }
}

export const callSchedule = (data) => {
    return {
        type: SCHEDULE_FETCH,
        data
    }
}

export const getAvatar = (data) => {
    return {
        type: AVATAR_FETCH,
        data
    }
}
