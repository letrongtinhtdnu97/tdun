export const HOME_FETCH ='@home/fetch'
export const HOME_SUCCESS ='@home/success'
export const HOME_FAIL ='@home/fail'

//export const HOME_FETCH ='@register/call'

export const callFetchStatus = (data) => {
    return {
        type: HOME_FETCH,
        data
    }
}