export const AUTH_FETCH_DATA = '@auth/data'
export const AUTH_FETCH_SUCCESS = '@auth/success'
export const AUTH_FETCH_FAIL = '@auth/fail'


export const AUTH_LOGOUT = '@auth/logout'

