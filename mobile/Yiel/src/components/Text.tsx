import React from 'react';
import {Text as RNText, TextProps, StyleSheet} from 'react-native'
import colors from '../constants/colors';
export const Text = (props: TextProps) => {
    return (
        <RNText {...props} style={[styles.text, props.style]}/>
    );
};

const styles = StyleSheet.create({
    text: {
        color: colors.textColor,
        fontSize:14
    }
})
