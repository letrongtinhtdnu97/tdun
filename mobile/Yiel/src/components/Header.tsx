import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native'
//import { TouchableOpacity } from './TouchableOpacity'
import {Text} from './Text'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import colors from '@src/constants/colors'
type HeaderProps = {
    leftIcon?: string;
    onPressLeft?: () => void;
    rightIcon?: string;
    onPressRight?: () => void;
    title?: string;
    backgroundColor?: string;
    borderBottomWidth?: number | 0;
}
export const Header = (props: HeaderProps) => {
    const {leftIcon, rightIcon, onPressLeft, onPressRight,title,backgroundColor,borderBottomWidth} = props
    console.log(props)
    return (
        <View style={[styles.header,{backgroundColor,borderBottomWidth}]} >
            <TouchableOpacity 
                style={styles.leftButton} 
                disabled={!onPressLeft} 
                onPress={onPressLeft} >
            {!!leftIcon && <Icon name={leftIcon} size={30} color="#000" />}
            </TouchableOpacity>
            {!!title && <Text style={styles.title}>{title}</Text> }
            <TouchableOpacity style={styles.rightButton} disabled={!onPressRight} onPress={onPressRight} >
                {!!rightIcon && <Icon name={rightIcon} size={30} color="#000" />}
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        height:50, 
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'space-between',
        //borderBottomWidth:0,
        borderBottomColor:'#ccc',
        backgroundColor: colors.navigationBarColor

    },
    leftButton: {
        width:60,
        paddingLeft:16,
    },
    title: {
        flex: 1,
        textAlign:'center',
        fontWeight:'bold',
        fontSize:20,
        textTransform: 'uppercase',
        color: colors.primary
    },
    leftIcon: {},
    rightButton: {
        width:60,
        paddingLeft:20
    },
    rightIcon: {}
})
