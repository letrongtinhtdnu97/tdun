import React from 'react';
import {View, Platform , StyleSheet, Text} from 'react-native';


const isAndroid = Platform.OS  === 'android'
const StatusBarHeight = isAndroid ? 0 : 20

const NavigationBarHeight = 58;
export const bottomSpacing = 34
export const Container = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.statusBar} />
            {props.children}
        </View>
    );
};

const styles = StyleSheet.create({
    statusBar: {
        height: StatusBarHeight,
        backgroundColor:'#fff'
    },
    container: {
       flex:1,
       backgroundColor: '#fff'
    }
})
