import React , {useEffect, useState} from 'react';
import {View, ActivityIndicator, Text} from 'react-native';
import NavigationService from '@src/navigation/NavigationService';
import LoadingHub from '@src/navigation/LoadingHub'
const Splash = () => {
    const [loading, setLoading] = useState(true)
    
    if(!loading){
        LoadingHub.show()
    }
    else{
        LoadingHub.hide()
    }
    return (
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <Text>Hello xin chao</Text>
        </View>
    );
    
};

export default Splash;