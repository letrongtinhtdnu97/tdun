import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { withNamespaces } from 'react-i18next'
import Screens  from './Screens'
import NavigationService from './NavigationService'
//
import Splash from '@src/screens/splash'
import SignUp from '@src/screens/authencation/SignUp'
import Login from '@src/screens/authencation/Login'

import LoadingHud from './LoadingHub';
import Tabbar from './Tabbar';

const AuthenticationStack = createStackNavigator({
    [Screens.LOGIN_SCREEN]: Login,
    [Screens.SIGN_UP_SCREEN]: SignUp
},{
   headerMode: 'none'
}

);
const AppTab = createBottomTabNavigator({
    [Screens.CONVERSATION_SCREEN]: Login,
    [Screens.USERS_SCREEN]: Login
},
    {
        tabBarComponent: ({navigation}) => <Tabbar navigation={navigation}  />
    }
)
const SwitchNavigator = createAppContainer(
    createSwitchNavigator({
        [Screens.SPLASH_SCREEN]:Splash,
        [Screens.AUTHENCATION_STACK]: AuthenticationStack,
        [Screens.APP_TAB]: AppTab
    })
);
 
class WrappedStack extends React.Component {
    static router = SwitchNavigator.router
    render() {
        const { t } = this.props
        return(
            <SwitchNavigator 
                // @ts-ignore
                ref={(navigatorRef: any) => {
                    NavigationService.setTopLevelNavigator(navigatorRef);
                }}
                screenProps={{t}} {...this.props}
            />
        );
    }
    
}

// const ReloadAppOnLanguageChange = withNamespaces('common', {
//     bindI18n: 'LanguageChanged',
//     bindStore: false,
// })(WrappedStack);
const AppRoot = () => {
    return (
        <React.Fragment>
            <WrappedStack />
            {/* <ReloadAppOnLanguageChange /> */}
            <LoadingHud />
        </React.Fragment>
    )
}
export default AppRoot