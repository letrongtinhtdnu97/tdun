import { NavigationActions} from 'react-navigation'

let _navigator: any = null;


class NavigationService {
    setTopLevelNavigator(navigatorRef: any){
        _navigator = navigatorRef
    }
    goBack = () => {
        _navigator.dispatch(NavigationActions.back());
    }
    navigate(routeName: string, params?: any | null) {
        _navigator.dispatch(
            NavigationActions.navigate({
                routeName,
                params
            })
        )
    }
}

export default new NavigationService();