import React from 'react';
import {View, Image, StyleSheet,TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Colors from '@src/constants/colors'

import Screens from './Screens'

type TabbarProps = {
    navigation: any
}
const Tabs = [
    {
        icon: 'camera',
        screen: Screens.CONVERSATION_SCREEN
    },
    {
        icon: 'chat',
        screen: Screens.USERS_SCREEN
    },
    
]
const Tabbar = (props: TabbarProps)  => {
    const currentIndex = props.navigation.state.index;
    const tabs = props.navigation.state.routes;
    const currentTab = tabs[currentIndex];
    const currentTabName = currentTab.routeName
    console.log('currentIndex',currentIndex)
    return (
        <View style={styles.container}>
            <View style={styles.tabBar}>
                
                {
                   Tabs.map(tab => (
                    <TouchableOpacity key={tab.screen} onPress={() => props.navigation.navigate(tab.screen)}  style={styles.chatButton}>
                        <Icon name={tab.icon} size={30} color={currentTabName === tab.screen ? Colors.primary: '#ccc'}/>
                    </TouchableOpacity>
                   ))
                }
                
            </View>
            <TouchableOpacity style={styles.plusButton}>
                <Icon name='plus' size={30} color='#fff' />
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        height:85,
    },
    tabBar: {
        height:60,
        marginTop:25,
        flexDirection:'row',
        backgroundColor:'#fbfcff',
        borderWidth: 0.5,
        borderColor: '#d4d4d4'
    },
    chatButton: {
        flex: 1,
        alignItems:'center',
        justifyContent: 'center',
    },
    chatIcon: {
        tintColor: '#c1c1c1'
    },
    userButton: {
        flex: 1,
        alignItems:'center',
        justifyContent: 'center',
    },
    userIcon: {
        tintColor: '#c1c1c1'
    },
    plusButton: {
        position: 'absolute',
        alignSelf:'center',
        top:0,
        width:50,
        height:50,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:25,
        backgroundColor: Colors.primary
    },
    plusIcon: {}

})
export default Tabbar;