export default {
    primary: '#6274e6',
    navigationBarColor: '#fff',
    welcomeMessage: '#5b5a5a',
    inputPlaceHolder: '#979899',
    buttontext: '#fff',
    action: '#ff7260',
    textColor: '#000',
    borderColor: '#f5f6f7'
}