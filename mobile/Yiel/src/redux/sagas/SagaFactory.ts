import AppAction from "../actions/AppAction"

import { takeEvery } from "redux-saga/effects"
import searchDeals from "./AppSaga"

function* dataSaga() {
  yield takeEvery(AppAction.GET_DEALS, searchDeals)
}

export default dataSaga