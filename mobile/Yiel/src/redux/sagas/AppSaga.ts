import { put } from 'redux-saga/effects'

import AppAction from '../actions/AppAction'
import UtilAction from '../actions/UtilAction'
import IAction from '../actions/IAction'

function* searchDeals(action: IAction<string, any>) {
  try {
    if (action.payload !== undefined && action.payload!.length > 0) {
      const data = 123
      yield put({ type: AppAction.DEALS_LOADED, data: data })
    } else {
      const data = 321
      yield put({ type: AppAction.DEALS_LOADED, data: data })
    }
  } catch (e) {
    yield put({ type: UtilAction.ERROR, error: "Cannot load deals" })
  }
}

export default searchDeals