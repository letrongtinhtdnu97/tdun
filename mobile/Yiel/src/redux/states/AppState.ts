
import IState from "./IState"

export default interface AppState extends IState {
    
    currentDealId: string | null,
    searchTerm?: string
  }