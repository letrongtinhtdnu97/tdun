export default {
    login: {
        welcome: 'welcome',
        email: 'email',
        password: 'password',
        forgot_password: 'forgot password',
        sign_in: 'Sign In',
        no_account: 'Dont have a account?',
        sign_up: 'Create new account',
    },
    signup: {
        title: 'Create new account',
        last_name: 'Last Name',
        first_name: 'First Name',
        email: 'email',
        password: 'password',
        sign_up: 'Sign Up',
        term: 'By tapping "Sign up" you agree\n to the [terms & conditions]'
    },
    chat: {
        title: 'Chat'
    }
}