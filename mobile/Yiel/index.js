/**
 * @format
 */
import "reflect-metadata";
import 'react-native-gesture-handler';
import React from 'react'
import  {AppRegistry} from 'react-native';
import AppRoot from './src/navigation/index';
import {name as appName} from './app.json';

import { Provider } from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './src/redux/reducers/ReducerFactory'
import dataSaga from './src/redux/sagas/SagaFactory'
const sagaMiddleware = createSagaMiddleware()

function configureStore() {
    const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
    sagaMiddleware.run(dataSaga)
    return store
  }

const store = configureStore()

const App = () => (
    <Provider store={store}>
        <AppRoot />
    </Provider>
);
AppRegistry.registerComponent(appName, () => App);
